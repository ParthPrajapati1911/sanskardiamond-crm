<?php

class department_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function record_count() {

        return $this->db->count_all("department");
    }

    // function fetch_department($limit, $start) {

    function fetch_department() {

        // $this->db->limit($limit, $start);
        $query = $this->db->get("department");

     
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;
            }

            return $data;
        }

        return false;
    }
    function fetch_departmentNew() {

        $query = $this->db->get("department");

     
        if ($query->num_rows() > 0) {

            return $query->result_array(); 
        }

        
    }
    function fetch_department_by_id($department_id) {
        $query = $this->db->get_where('department',array('department_id'=>$department_id));
        return $query->row();
    }

    function get_department_rate($department_id){
        $query = $this->db->get_where('department',array('department_id'=>$department_id));
        return $query->row();
    }

    function employee_existence($employee_id) {

        $query = $this->db->get_where('salary',array('employee_id'=>$employee_id));

        if($query->row() > 0){

            return true;
        }

        return false;
    }

    function save_department($data) {

        $this->db->insert('department', $data);
    }

    function update_department_by_id($department_id,$data) {

        $this->db->where('department_id', $department_id);
        $this->db->update('department', $data);
    }

    function update_salary_status($employee_id) {

        $this->db->where('employee_id', $employee_id);
        $this->db->update('employee', array('employee_salary'=>1));
    }

    function erase_department($department_id) {
        $this->db->delete('department', array('department_id' => $department_id));
    }

} 