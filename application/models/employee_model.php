<?php

class Employee_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function record_count() {

        return $this->db->count_all("employee");
    }

    function manager_record_count() {

        return $this->db->count_all("manager");
    }

    function gaatEmployee_record_count() {

        return $this->db->count_all("manager");
    }
    function Pf_employee_record_count() {

        return $this->db->count_all("Pf_employee");
    }

    // function fetch_employee($limit, $start) {
    function fetch_employee() {

        // $this->db->limit($limit, $start);
        $query = $this->db->get("employee");

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;
            }

            return $data;
        }

        return false;
    }
    
    function fetch_Pf_employee($limit, $start) {

        // $this->db->limit($limit, $start);
        // $this->db->join("employee","right");
        // $query = $this->db->get("Pf_employee");
        $this->db->limit($limit, $start);
        $this->db->select('*')
         ->from('Pf_employee')
         ->join('employee', 'Pf_employee.employee_id = employee.employee_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;
            }

            return $data;
        }

        return false;
    }
    function fetch_Pf_employeeNew() {

        // $this->db->limit($limit, $start);
        // $this->db->join("employee","right");
        // $query = $this->db->get("Pf_employee");
        //$this->db->limit($limit, $start);
        $this->db->select('*')
         ->from('Pf_employee')
         ->join('employee', 'Pf_employee.employee_id = employee.employee_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }
    function fetch_manager($limit, $start) {

        $this->db->limit($limit, $start);
        $query = $this->db->get_where("employee",array("type"=>"Manager"));

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    function fetch_gatEmployee($limit, $start) {

        $this->db->limit($limit, $start);
        $query = $this->db->get_where("employee",array("type"=>"GaatEmployee"));

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;
            }

            return $data;
        }

        return false;
    }
    
    function fetch_employee_by_id($employee_id) {

        $query = $this->db->get_where('employee',array('employee_id'=>$employee_id));

        return $query->row();
    }

    function fetch_employeePf_by_Id($employeePf_id) {

        $query = $this->db->get_where('Pf_employee',array('Pf_employee_Id'=>$employeePf_id));

        return $query->row();
    }
    function fetch_Pfemployee_by_id($employee_id){
        $query = $this->db->get_where('Pf_employee',array('Pf_employee_Id'=>$employee_id));

        return $query->row();
    }
    function fetch_manager_by_id($manager_id) {

        $query = $this->db->get_where('employee',array('employee_id'=>$manager_id,"type"=>'Manager'));

        return $query->row();
    }
    function fetch_GatEmployee_by_id($employee_id) {

        $query = $this->db->get_where('employee',array('employee_id'=>$employee_id,"type"=>'GaatEmployee'));

        return $query->row();
    }
    function fetch_employee_by_department($department_id){

        $this->db->select("employee_id,employee_name");
        $query = $this->db->get_where('employee',array('employee_department'=>$department_id));
        return $query->result();

    }

    function fetch_gat_emp(){

        $this->db->select("employee_id,employee_name");
        $query = $this->db->get_where('employee',array('type'=>'GaatEmployee'));
        return $query->result();

    }

    function check_diamond_Available($Employee_id,$date){
        // echo $Employee_id ."ddddddD". $date;
        //return "".$Employee_id."".$Department_id."".$date;
        $this->db->select("today_diamond");
        $query = $this->db->get_where('dailywork',array('employee_id'=>$Employee_id,"date"=>$date));
        // print_r($Employee_id);exit;
        if($query->num_rows()>0){
            //return "yes";
            return $query->result();
        }else{
            //return "no";
            return 0;
        }
    }
    function check_diamond_AvailableNew($Employee_id,$Department_id,$date){
        
        //return "".$Employee_id."".$Department_id."".$date;
        $this->db->select("today_diamond");
        $query = $this->db->get_where('gat_dailywork',array('employee_id'=>$Employee_id,"date"=>$date,'department_id'=>$Department_id));
        if($query->num_rows()>0){
            //return "yes";
            return $query->result();
        }else{
            //return "no";
            return 0;
        }
    }
    function fetch_employee_by_department_setTable($department_id){

        $this->db->select("*");
        $query = $this->db->get_where('employee',array('employee_department'=>$department_id));
        return $query->result();

    }
    function fetch_gat_reportData_twoDate($start_date,$end_date){
        $this->db->select('employee.employee_id,employee.employee_name,employee.employee_city,employee.employee_designation,emp_otherpay.payment,emp_otherpay.lastDebit_FixDeposite,emp_otherpay.totalBaki_Debit,sum(gat_salary.diamond) as diamond,sum(gat_salary.salary) as salary');
        $this->db->where('start_date >=', $start_date);
        $this->db->where('end_date <=', $end_date);
        $this->db->join('employee', 'employee.employee_id = gat_salary.employee_id');
        $this->db->join('emp_otherpay', 'emp_otherpay.employee_id = employee.employee_id',"left");
        $this->db->group_by("employee.employee_id");
        $query = $this->db->get('gat_salary');
        if($query->num_rows()){
             return $query->result_array();
        }else{
             return array();
        }
    }
    
    function fetch_reportData_twoDate($start_date,$end_date,$department_id){
        $this->db->select('employee.employee_id,employee.employee_name,employee.employee_city,employee.employee_designation,emp_otherpay.payment,emp_otherpay.lastDebit_FixDeposite,emp_otherpay.totalBaki_Debit,salary.diamond,sum(salary.salary) as salary');
        $this->db->where('salary.start_date >=', $start_date);
        $this->db->where('salary.end_date <=', $end_date);
        $this->db->where('employee.employee_department =', $department_id);
        $this->db->join('employee', 'employee.employee_id = salary.employee_id');
        $this->db->join('emp_otherpay', 'emp_otherpay.employee_id = employee.employee_id',"left");
        $this->db->group_by("salary.employee_id");
        $query = $this->db->get('salary');
        if($query->num_rows()){
             return $query->result();
        }else{
             return array();
        }
    }

    function fetch_reportData_twoDate_New($start_date,$end_date){
        $this->db->select('employee.employee_id,
        employee.employee_name,
        employee.employee_city,
        employee.employee_designation,
        department.department_name,
        emp_otherpay.payment,
        emp_otherpay.lastDebit_FixDeposite,
        emp_otherpay.totalBaki_Debit,
        gat_salary.diamond,
        gat_salary.salary');
        $this->db->where('gat_salary.start_date >=', $start_date);
        $this->db->where('gat_salary.end_date <=', $end_date);
        //$this->db->where('gat_salary.employee_id =', 'employee.employee_id');
        $this->db->join('employee', 'employee.employee_id = gat_salary.employee_id');
        $this->db->join('emp_otherpay', 'emp_otherpay.employee_id = employee.employee_id',"left");
        $this->db->join('department', 'department.department_id = gat_salary.department_id',"left");
        //$this->db->group_by("gat_salary.employee_id");
        $query = $this->db->get('gat_salary');
        
        $res = $query->result_array();

        //echo "<pre>";print_r($res);
        $employee_id = $res[0]['employee_id'];
        $diamond=0;
        $salary=0;
        $diamond1=0;
        $salary1=0;
        $department_name="";
        // for($i=0;$i<count($res)-1;$i++){
        //     $employee_id[] = $res[$i]['employee_id'];
        //     if($employee_id[$i] == $res[$i+1]['employee_id']){
        //         $employee_id[] = $res[$i+1]['employee_id'];
        //         $i=$i+1;
        //     }
        //     print_r($employee_id);
        // }
        $data = array();
        $c= count($data);
        for($i=0;$i<=count($res)-1;$i++){
            if($res[$i]["employee_id"]==$employee_id) {
                $employee[]=$res[$i+1]["employee_id"];
            }
        }
        
        for($i=0;$i<=count($res)-1;$i++){
            
            if($res[$i]["employee_id"]==$employee_id) {
                 $data[$c]['employee_id']=$employee_id;
                 $data[$c]['employee_name']=$res[$i]['employee_name'];
                 $data[$c]['employee_city']=$res[$i]['employee_city'];
                 $data[$c]['payment']=$res[$i]['payment'];
                 $data[$c]['lastDebit_FixDeposite']=$res[$i]['lastDebit_FixDeposite'];
                 $data[$c]['totalBaki_Debit']=$res[$i]['totalBaki_Debit'];

                 $data[$c]['department_name'][]=$res[$i]["department_name"];
                 $diamond=$diamond+$res[$i]["diamond"];
                 $data[$c]['total_diamond']=$diamond;
                 $data[$c]['diamond'][]=$res[$i]["diamond"];
                 $salary=$salary+$res[$i]["salary"];
                 $data[$c]['total_salary']=$salary;
                 $data[$c]['salary'][]=$res[$i]["salary"];
                
                 //$employee_id=$res[$i+1]["employee_id"];
            }else{
                $employee_id = $res[$i]["employee_id"];
                $c=$c+1;
                $data[$c]['employee_id']=$employee_id;
                 $data[$c]['employee_name']=$res[$i]['employee_name'];
                 $data[$c]['employee_city']=$res[$i]['employee_city'];
                 $data[$c]['payment']=$res[$i]['payment'];
                 $data[$c]['lastDebit_FixDeposite']=$res[$i]['lastDebit_FixDeposite'];
                 $data[$c]['totalBaki_Debit']=$res[$i]['totalBaki_Debit'];

                 $data[$c]['department_name'][]=$res[$i]["department_name"];
                 $diamond1=$diamond1+$res[$i]["diamond"];
                 $data[$c]['diamond'][]=$res[$i]["diamond"];
                 $data[$c]['total_diamond']=$diamond1;
                 $salary1=$salary1+$res[$i]["salary"];
                 $data[$c]['total_salary']=$salary1;
                 $data[$c]['salary'][]=$res[$i]["salary"];
            }
            // else{
            //     //  $c = count($data);
            //      $data[$c]['employee_id']=$res[$i]['employee_id'];
            //      $data[$c]['employee_name']=$res[$i]['employee_name'];
            //      $data[$c]['employee_city']=$res[$i]['employee_city'];
            //      $data[$c]['payment']=$res[$i]['payment'];
            //      $data[$c]['lastDebit_FixDeposite']=$res[$i]['lastDebit_FixDeposite'];
            //      $data[$c]['totalBaki_Debit']=$res[$i]['totalBaki_Debit'];

            //      $data[$c]['department_name'][]=$res[$i]["department_name"];
            //      $diamond=$diamond+$res[$i]["diamond"];
            //      $data[$c]['diamond'][]=$res[$i]["diamond"];
            //      $salary=$salary+$res[$i]["salary"];
            //      $data[$c]['salary'][]=$res[$i]["salary"];
                 
            // }
        }
        echo "<pre>";
        print_r($data);
        //print_r(array_count_values($employee));
       exit;
        if($query->num_rows()){
             return $data;
        }else{
             return array();
        }
    }
    
    function fetch_employee_dailywork($Employee_id){
        $month = date('M');
        $this->db->select("SUM(today_diamond) total_diamond");
        $query = $this->db->get_where('dailywork',array("employee_id"=>$Employee_id,"month"=>$month));
        return $query->result();
    }
    function get_manager_rate($Employee_id){
        $this->db->select("rate");
        $type = $this->db->get_where('employee',array("employee_id"=>$Employee_id));
        return $type->result();
        // $query = $this->db->get_where('employee',array("employee_id"=>$Employee_id,"type"=>array("Manager","GaatEmployee")));
        // return $query->result();
    }
    function get_gatEmployee_rate($Employee_id){
        $this->db->select("rate");
        $query = $this->db->get_where('employee',array("employee_id"=>$Employee_id,"type"=>"GaatEmployee"));
        return $query->result();
    }
    function fetch_employee_salary($data){
        $this->db->select("SUM(today_diamond) total_diamond");
        $this->db->where('date >=', $data['start_date']);
        $this->db->where('date <=', $data['end_date']);
        $query = $this->db->get_where('dailywork',array("employee_id"=>$data['Employee_id'],"paid"=>'0'));
        if($query){
            return $query->result();
        }else{
            return 0;
        }
        // $this->db->select("total_diamond");
        // $query = $this->db->get_where('dailywork',array("employee_id"=>$data['Employee_id']));
        // return $query->result();
    }
    function fetch_gat_employee_salary($data){
        //print_r($data);exit;
        $this->db->select("gat_dailywork.today_diamond total_diamond, gat_dailywork.department_id,department.department_name,department.department_rate");
        $this->db->where('gat_dailywork.date >=', $data['start_date']);
        $this->db->where('gat_dailywork.date <=', $data['end_date']);
        $this->db->join('department', 'department.department_id = gat_dailywork.department_id',"left");
        // $this->db->group_by("gat_dailywork.department_id");
        //$this->db->join('emp_otherpay', 'emp_otherpay.employee_id = employee.employee_id',"left");
        
        $query = $this->db->get_where('gat_dailywork',array("employee_id"=>$data['Employee_id'],"paid"=>'0'));
        
        if($query){
            return $query->result_array();
        }else{
            return 0;
        }
        //"department_id"=>$data['Department_id'],
        // $this->db->select("total_diamond");
        // $query = $this->db->get_where('dailywork',array("employee_id"=>$data['Employee_id']));
        // return $query->result();
    }
    function employee_existence($employee_id) {

        $query = $this->db->get_where('salary',array('employee_id'=>$employee_id));

        if($query->row() > 0){

            return true;
        }

        return false;
    }

    function save_employee($data) {

        $this->db->insert('employee', $data);
    }
    function save_employee_Pf($data) {

        $this->db->insert('monthwise_emppf', $data);
    }
    function save_Pf_employee($data) {

        $this->db->insert('Pf_employee', $data);
        //mysqli_rollback($this->db);
    }
    
    function save_manager($data) {

        $this->db->insert('employee', $data);
    }
    function save_gatEmployee($data) {

        $this->db->insert('employee', $data);
    }
    function add_extraPay($data){
        $this->db->where('employee_id', $data['employee_id']);
        $query = $this->db->get('emp_otherpay');
        
        if($query->num_rows()){
            $row = $query->result_array();
            $this->db->where('employee_id', $data['employee_id']);
            $this->db->update('emp_otherpay', array('payment' => $row[0]['payment']+$data['payment']));
        }else{
            $this->db->insert('emp_otherpay', $data);
        }
        
    }
    function update_manager_by_id($manager_id,$data) {

        $this->db->where('employee_id', $manager_id);
        $this->db->update('employee', $data);
    }
    function update_gaatEmployee_by_id($employee_id,$data) {

        $this->db->where('employee_id', $employee_id);
        $this->db->update('employee', $data);
    }
    function update_employee_by_id($employee_id,$data) {

        $this->db->where('employee_id', $employee_id);
        $this->db->update('employee', $data);
    }
    function update_pfemployee_by_id($employeePf_id,$data) {

        $this->db->where('Pf_employee_Id', $employeePf_id);
        $this->db->update('Pf_employee', $data);
    }
    
    function update_salary_status($employee_id) {

        $this->db->where('employee_id', $employee_id);
        $this->db->update('employee', array('employee_salary'=>1));
    }

    function erase_employee($employee_id) {
            $this->db->delete('employee', array('employee_id' => $employee_id));
        }
    function erase_Pfemployee($employeePf_id) {
            $this->db->delete('Pf_employee', array('Pf_employee_Id' => $employeePf_id));
    }
        
    function erase_manager($manager_id) {
            $this->db->delete('employee', array('employee_id' => $manager_id));
        }  
    function erase_gatEmployee($employee_id) {
            $this->db->delete('employee', array('employee_id' => $employee_id));
    }   
    function Employee_ExtraPay_record_count(){
        //$query = $this->db->get('emp_otherpay');
        $this->db->select('*')
         ->from('emp_otherpay')
         ->join('employee', 'employee.employee_id = emp_otherpay.employee_id');
        $query = $this->db->get();
        if($query){
            return $query->result_array();
        }else{
            return array();
        }
    }
    function get_EmployeeBaki($employee_id){
        $this->db->select('sum(payment) as baki');
        $this->db->where('employee_id', $employee_id);
        $this->db->group_by("employee_id");
        $query = $this->db->get('emp_otherpay');
        if($query){
            return $query->result_array();
        }else{
            return 0;
        }
    }
    function get_EmployeePf($employee_id){
        $lastmonth=date('M', strtotime('-1 month'));
        $secoundlastmonth=date('M', strtotime('-2 month'));
        $array = array('employee_id' => $employee_id, 'month' => $lastmonth, 'debited' => 0);
        $this->db->select('*');
        $this->db->where($array);
        $query = $this->db->get('monthwise_emppf');
        if($query){
            $data = $query->result_array();
            if(empty($data)){
                $array = array('employee_id' => $employee_id, 'month' => $secoundlastmonth, 'debited' => 0);
                $this->db->select('*');
                $this->db->where($array);
                $query = $this->db->get('monthwise_emppf');
                if($query){
                    return $query->result_array();
                }else{
                    return array();
                }
            }
            return $query->result_array();
        }else{
            return array();
        }
    }
    function update_EmployeePf_status($employee_id){
        $lastmonth=date('M', strtotime('-1 month'));
        $array = array('employee_id' => $employee_id, 'month' => $lastmonth, 'debited' => 0);
        $this->db->where($array);
        $query = $this->db->update('monthwise_emppf', array('debited' => 1));
        if($query){
            return 1;
        }else{
            return 0;
        }
    }
    // function trans_rollback(){
    //     $mysqli = new mysqli("localhost", "root", "", "payroll_test");
    //     $mysqli->rollback();
    //     if($mysqli->rollback()){
    //         echo "done".$mysqli->rollback();
    //     }else{
    //     echo "false";
    //     }
    // }
    function update_EmployeeBaki($employee_id,$fixDeposite){

        $this->db->where('employee_id',$employee_id);
        $query = $this->db->get('emp_otherpay');
        
        if($query->num_rows()){
            $row = $query->result_array();
            if($row[0]['payment']<$fixDeposite){
                $fixDeposite=$fixDeposite;
                $payment=$row[0]['payment']-$fixDeposite;
                //$fixDeposite=$fixDeposite-$row[0]['payment'];
                $this->db->where('employee_id', $employee_id);
                $query = $this->db->update('emp_otherpay', array('payment' => $payment,"lastDebit_FixDeposite" => $row[0]['payment'],"totalBaki_Debit" => $row[0]['totalBaki_Debit']+$row[0]['payment']));
            }else{
                $this->db->where('employee_id', $employee_id);
                $query = $this->db->update('emp_otherpay', array('payment' => $row[0]['payment']-$fixDeposite,"lastDebit_FixDeposite" => $fixDeposite,"totalBaki_Debit" => $row[0]['totalBaki_Debit']+$fixDeposite));
            }
            if($query){
                return 1;
            }else{
                return 0;
            }
        }
    }

    function delete_employee($employee_id){
        $this->db->delete('employee', array('employee_id' => $id));
    }

    function add_employee_salary($salary_data){
        $this->db->insert('salary', $salary_data);
        for($i=$salary_data['start_date'];$i<=$salary_data['end_date'];$i++){
            $this->db->where('employee_id', $salary_data['employee_id']);
            $this->db->where('date =', $i);
            $this->db->update('dailywork', array('paid'=>'1'));
            $this->db->update('salary', array('paid'=>'1'));
        }
    }
    function add_gat_salary($salary_data){
        $this->db->insert('gat_salary', $salary_data);
        for($i=$salary_data['start_date'];$i<=$salary_data['end_date'];$i++){
            $this->db->where('employee_id', $salary_data['employee_id']);
            $this->db->where('date =', $i);
            $this->db->update('gat_dailywork', array('paid'=>'1'));
            $this->db->update('gat_salary', array('paid'=>'1'));
        }
    }
    function add_employee_dailyWork($data){
            $this->db->insert('dailywork', $data);
    }
    function add_gat_employee_dailyWork($data){
        $this->db->insert('gat_dailywork', $data);
}

    function edit_employee_dailyWork($data){ 
            $this->db->where('employee_id', $data['employee_id']);
            //$this->db->where('department_id', $data['department_id']);
            $this->db->where('date',$data['date']);
            $this->db->update('dailywork', array('today_diamond'=>$data['today_diamond'],'total_diamond'=>$data['today_diamond']));
    }
    function edit_employee_dailyWorkNew($data){ 
        $this->db->where('employee_id', $data['employee_id']);
        $this->db->where('department_id', $data['department_id']);
        $this->db->where('date',$data['date']);
        $this->db->update('gat_dailywork', array('today_diamond'=>$data['today_diamond'],'total_diamond'=>$data['today_diamond']));
}

    function searchEmployee($query){
        $output = '';     
        $result = $this->db->select('*')->from('employee')
       ->where("employee_name LIKE '%$query%'")->get();
       $data = $result->result_array();
       // print_r($result);
            $output = '<ul class="list-unstyled">';  
            if(count($data) > 0)  
            {  //print_r($data);
                foreach($data as $row)  
                {  //print_r($row["employee_name"]);
                        $output .= '<li>'.$row["employee_name"].'</li>';  
                }  
            }  
            else  
            {  
                $output .= '<li>Employee Not Found</li>';  
            }  
            echo $output .= '</ul>';  
    }

    function searchemployee1($query){
        $result = $this->db->select('*')->from('employee')
        ->where("employee_name LIKE '%$query%'")->get();
         $data = $result->row_array();
         print_r(json_encode($data));
    }

    /**For get departmnet name bu department id */
    function getDepartmentName($department_id){
        $result = $this->db->select('department_name')->from('department')
        ->where("department_id",$department_id)
        ->get();
         $data = $result->row_array();
         return $data['department_name'];
    }
} 