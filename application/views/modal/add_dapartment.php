<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="myModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Department</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                    <form class="form-validate form-horizontal" id="feedback_form" action="<?php echo base_url(); ?>department/add_department.html" method="post">
                        <div class="form-group">
                            <label for="name" class="control-label col-lg-2">Department Name</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="department_name" type="text" required/>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-2">Rate</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="rate" type="text" required/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Add New</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
