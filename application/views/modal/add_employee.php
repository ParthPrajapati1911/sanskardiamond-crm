<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="myModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Employee</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                    <form class="form-validate form-horizontal" id="feedback_form" action="<?php echo base_url(); ?>employee/add_employee.html" method="post">
                        <div class="form-group">
                            <label for="name" class="control-label col-lg-2">Full Name</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="name" type="text" required/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="type" class="control-label col-lg-2">Department</label>
                            <div class="col-lg-10">
                                <select class="form-control" name="department" required>
                                <option value="">All Selected</option>
                                    <?php $c=0; foreach($department as $data[$c]){
                                                $res=array($data[$c]);
                                                foreach ($res as $key => $value) {
                                                    $result['department_name'] = $value->department_name; 
                                                    $result['department_id'] = $value->department_id;
                                                    ?>
                                        <option value="<?php echo $result['department_id'] ;?>"><?php echo $result['department_name'] ;}?></option>

                                    <?php $c++; } ?>
                                </select>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-2">Phone</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="phone" type="text" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label col-lg-2">City</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" name="city" required></textarea>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="address" class="control-label col-lg-2">Password</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="password" value="123456789" required></input>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Add New</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
