<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="myModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add PF Employee</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                    <form class="form-validate form-horizontal" id="feedback_form" action="<?php echo base_url(); ?>employee/add_Pfemployee.html" method="post">
                        
                    <div class="form-group">
                            <label for="type" class="control-label col-lg-2">Department</label>
                            <div class="col-lg-10">
                                <select class="form-control department" name="department" id="department1" >
                                <option value="">All Selected</option>
                                    <?php $c=0; foreach($department as $data[$c]){
                                                $res=array($data[$c]);
                                                foreach ($res as $key => $value) {
                                                    $result['department_name'] = $value->department_name; 
                                                    $result['department_id'] = $value->department_id;
                                                    ?>
                                        <option value="<?php echo $result['department_id'] ;?>"><?php echo $result['department_name'] ;}?></option>

                                    <?php $c++; } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="control-label col-lg-2">Employee</label>
                            <div class="col-lg-10">
                                <select class="form-control" name="Employee" id="Employee" required>
                                <option value="">All Selected</option>
                                   
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-2">PF No</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="pf_no" type="number" required/>
                            </div>
                        </div>
                      
                       
                     <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Add New</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var c=0;
$(document).ready(function(){
$("#department1").change(function(){
        var department_id = $(this).children("option:selected").val();
        //alert("You have selected the country - " + department_id);
        if(department_id){
            $.ajax({
                type:'POST',
                url:"<?php echo base_url(); ?>employee/get_employee_dataNew/"+department_id, 
                
                success:function(data){
                    console.log('SUCCESS: ', data);
                    
                    $('#Employee')
                            .html(data);
                    },
            
                error: function(data) {
                            console.log('ERROR: ', data);
                        },
                
            }); 

        }
    });
});
</script>
