<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<?php require_once("modal/add_dapartment.php"); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <a class="btn btn-primary pull-left" href="" data-toggle="modal" data-target="#myModal" id="add">Add New</a>
        </div>
    </div>
</div>

<div class="row">
<?php if($this->session->flashdata('addDepartment')){?>
    <!-- <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Department Added.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div> -->
    <?php
    echo '
    <script>swal("Good job!", "Department has been added successfully!", "success");</script>';
    ?>
<?php }?>
<?php if($this->session->flashdata('deleteDepartment')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Department Deleted.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('editDepartment')){?>
    <!-- <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Department Edited.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div> -->
    <?php
    echo '
    <script>swal("Good job!", "Department has been updated successfully!", "success");</script>';
    ?>
<?php }?>

    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-responsive table-striped table-advance table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th><i class="icon_profile"></i> Id </th>
                            <th><i class="icon_profile"></i> Department Name</th>
                            <th><i class="icon_datareport"></i> Rate</th>
                            <th><i class="icon_datareport"></i> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($results as $list){ ?>
                        <tr>
                            <td><?php echo $list->department_id; ?></td>
                            <td>
                                <?php echo $list->department_name; ?>
                            </td>
                            <td><?php echo $list->department_rate; ?></td>
                            <td>
                                <a href="<?php echo base_url(); ?>department/update_department/<?php echo $list->department_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:green;font-size:20px"></i></a>           
								|
								<!-- <a  href="<?php echo base_url(); ?>department/delete_department/<?php echo $list->department_id; ?>"><i class="fa fa-trash-o" aria-hidden="true" style="color:red;font-size:20px"></i></a> -->
                                <a onclick="deleteDepartment(<?=$list->department_id?>)"><i class="fa fa-trash-o" aria-hidden="true" style="color:red;font-size:20px"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php echo $links; ?></p></div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
// swal({
//         title: "Are you sure?",
//         text: "You will not be able to recover this imaginary file!",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonClass: "btn-danger",
//         confirmButtonText: "Yes, delete it!",
//         cancelButtonText: "No, cancel plx!",
//         closeOnConfirm: false,
//         closeOnCancel: false
//       }
// );
});
function deleteDepartment(departmentId) {
    swal({
        title: "Are you sure?",
        text: "You are sure to delete this department!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plz!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {
            $.ajax({  
                    url:"<?php echo base_url(); ?>department/delete_department/"+departmentId,  
                    method:"POST",  
                    data:{departmentId:departmentId},  
                    success:function(data)  
                    {  
                        swal("Deleted!", "Your department has been deleted.", "success");
                        setTimeout(() => {
                        location.reload();
                        }, 2000);
                    }  
            }); 
            
        } else {
            swal("Cancelled", "Your department is still available in department list :)", "error");
        }
        }
    );
        // $.ajax({  
        //         url:"<?php echo base_url(); ?>department/delete_department/"+departmentId,  
        //         method:"POST",  
        //         data:{query:query},  
        //         success:function(data)  
        //         {  
        //         }  
        // });    
}
</script>
