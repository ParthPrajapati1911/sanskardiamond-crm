<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<?php require_once("modal/add_Pfemployee.php"); ?>
<div class="row">
    <div class="col-lg-12 form-group">
        <div class="panel-body col-lg-1">
            <a class="btn btn-primary pull-left" href="" data-toggle="modal" data-target="#myModal" id="add">Add New</a>
        </div>
        <div class="panel-body col-lg-1">
            <a class="btn btn-primary pull-left" href="add_Pf_employee.html" data-toggle="" data-target="" id="">Manage PF</a>
        </div>
        <!-- <div class="panel-body col-lg-10">
            <select class="form-control department" name="department" id="department_id" >
                <option value="">All Selected</option>
                                        <?php $c=0; foreach($department as $data[$c]){
                                                    $res=array($data[$c]);
                                                    foreach ($res as $key => $value) {
                                                        $result['department_name'] = $value->department_name; 
                                                        $result['department_id'] = $value->department_id;
                                                        ?>
                                            <option value="<?php echo $result['department_id'] ;?>"><?php echo $result['department_name'] ;}?></option>

                                        <?php $c++; } ?>
            </select>
        </div> -->
    </div>
</div>

<div class="row">
<?php if($this->session->flashdata('addEmployeePf')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee PF is Added.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('deletePfEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Pf Employee Deleted.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('editPfEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Pf Employee Edited.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body table-responsive">
                <table class="table table-responsive table-striped table-advance table-hover" id="datatable">
                    <thead>
                        <tr>
                            <!-- <th><i class="icon_profile"></i> No </th>   -->
                            <th><i class="icon_profile"></i> ID </th>
                            <th><i class="icon_profile"></i> Employee Name </th>
                            <th><i class="icon_profile"></i> Designation</th>
                            <th><i class="icon_datareport_alt"></i> PF No</th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    // echo "<pre>";print_r($results);exit;
                    if(!empty($results)){foreach($results as $list){ ?>
                        <tr>
                            <!-- <td><?php echo $i; ?></td> -->
                            <td>
                                <?php echo $list->employee_id; ?>
                            </td>
                            <td><?php echo $list->employee_name; ?></td>
                            <td><?php if(empty($list->employee_designation)){
                                echo $list->employee_department;  
                            }else{
                                echo $list->employee_designation;  
                            }  ?></td>
                            <td><?php echo $list->Pf_no; ?></td>
                            <td>
                                <a href="<?php echo base_url(); ?>employee/updatePf_employee/<?php echo $list->Pf_employee_Id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:green;font-size:20px"></i></a>
                                
								|
								<a  href="<?php echo base_url(); ?>employee/deletePf_employee/<?php echo $list->Pf_employee_Id; ?>"><i class="fa fa-trash-o" aria-hidden="true" style="color:red;font-size:20px"></i></a>
                            </td>
                        </tr>
                    <?php } }?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php echo $links; ?></p></div>
    </div>
</div>
<script>
$(document).ready(function(){
     $("select.department").change(function(){
        var department_id = $(this).children("option:selected").val();
        //alert("You have selected the country - " + department_id);
       
        if(department_id){
            
            $.ajax({
                type:'POST',
                url:"<?php echo base_url(); ?>employee/get_employee_data_setTable/"+department_id, 
                
                success:function(html){
                    console.log('SUCCESS: ', html);
                    $('#employee_Data').html(html);
                    $.ajax({
                            type: "post",
                            url: "<?php echo base_url(); ?>department/get_department_rate/"+department_id,
                            success: function (rate) {
                                console.log(rate);

                                $.each($.parseJSON(rate), function (index, value) {
                                    
                                    $("#temp_salary").val(value['department_rate']);

                                });

                            }
                        });
                }
            });
        }
     });
});
</script>
