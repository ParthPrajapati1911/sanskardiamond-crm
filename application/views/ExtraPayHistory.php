<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<?php require_once("modal/add_employee.php"); ?>
<div class="row">
    <div class="col-lg-12 form-group">
        <div class="panel-body col-lg-2">
            <a class="btn btn-primary pull-left" href="<?php echo base_url(); ?>site/extra_pay.html" data-toggle="modal" data-target="" id="">Add New</a>
        </div>
        <!-- <div class="panel-body col-lg-10">
            <select class="form-control department" name="department" id="department_id" >
            <option value="">All Selected</option>
                                        <?php $c=0; foreach($department as $data[$c]){
                                                    $res=array($data[$c]);
                                                    foreach ($res as $key => $value) {
                                                        $result['department_name'] = $value->department_name; 
                                                        $result['department_id'] = $value->department_id;
                                                        ?>
                                            <option value="<?php echo $result['department_id'] ;?>"><?php echo $result['department_name'] ;}?></option>

                                        <?php $c++; } ?>
            </select>
        </div> -->
    </div>
</div>

<div class="row">
<?php if($this->session->flashdata('addEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Added.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('deleteEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Deleted.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('editEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Edited.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body table-responsive">
                <table class="table table-responsive table-striped table-advance table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th><i class="icon_profile"></i> Full Name</th>
                            <th><i class="icon_datareport_alt"></i> Department</th>
                            <th><i class="icon_datareport"></i> City </th>
                            <th><i class="icon_datareport"></i> Contact No </th>
                            <th><i class="icon_datareport"></i> Payment Amount </th>
                            <th><i class="icon_datareport"></i> LastDebit FixDeposite </th>
                            <th><i class="icon_datareport"></i> TotalBaki Debit </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    // echo "<pre>";print_r($result);exit;
                    $c=1;foreach($result as $list){ ?>
                        <tr>
                            <td><?php echo $list['employee_id']; ?></td>
                            <td>
                                <?php echo $list['employee_name']; ?>
                            </td>
                            <td><?php echo $list['employee_department']; ?></td>
                            <td><?php echo $list['employee_city']; ?></td>
                            <td><?php echo $list['employee_phone']; ?></td>
                            <td><?php echo $list['payment']; ?></td>
                            <td><?php echo $list['lastDebit_FixDeposite']; ?></td>
                            <td><?php echo $list['totalBaki_Debit']; ?></td>
                        </tr>
                    <?php $c++;} ?>
                </table>
            </div>
        </section>
        <!-- <div class="text-center"><p><?php echo $links; ?></p></div> -->
    </div>
</div>
<script>
$(document).ready(function(){
     $("select.department").change(function(){
        var department_id = $(this).children("option:selected").val();
        //alert("You have selected the country - " + department_id);
       
        if(department_id){
            
            $.ajax({
                type:'POST',
                url:"<?php echo base_url(); ?>employee/get_employee_data_setTable/"+department_id, 
                
                success:function(html){
                    console.log('SUCCESS: ', html);
                    $('#employee_Data').html(html);
                    $.ajax({
                            type: "post",
                            url: "<?php echo base_url(); ?>department/get_department_rate/"+department_id,
                            success: function (rate) {
                                console.log(rate);

                                $.each($.parseJSON(rate), function (index, value) {
                                    
                                    $("#temp_salary").val(value['department_rate']);

                                });

                            }
                        });
                }
            });
        }
     });
});
</script>
