<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<div class="row">
<?php if($this->session->flashdata('addEmployeePf')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Pf Added.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('deleteDailywork')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Dailywork Deleted.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('editDailywork')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Dailywork Edited.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="form"><?php //print_r($Pf_employee); ?>
                    <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url(); ?>employee/add_employee_Pf.html">
                            
                        <div class="form-group">
                            <label for="department" class="control-label col-lg-1">Search Name </label>
                            <div class="col-lg-11">
                            
                            <input type="text" class="form-control department" placeholder="Search employee name" name="Employee_name_search" id="Employee_name_search">
                            <div id="employeeList"></div>  
                            </div>
                        </div>

                        <div class="form-group" id="Employee" class="col-lg-12">
                          <div class="col-lg-6">
                            <label for="department" class="control-label col-lg-2">Employee ID </label>
                            <div class="col-lg-10">
                                <input class="form-control department" type="number" name="Employee_id" id="Employee_id" value="<?php echo $Pf_employee[0]['employee_id']; ?>">
                                <input class="form-control department" type="number" name="PfEmployee_id" id="PfEmployee_id" value="<?php echo $Pf_employee[0]['Pf_employee_Id']; ?>">
                            </div>
                          </div>
                          <div class="col-lg-6">
                          <label for="department" class="control-label col-lg-2">Employee Name </label>
                            <div class="col-lg-10">
                                <input class="form-control department" type="text" name="Employee_name" id="Employee_name" value="<?php echo $Pf_employee[0]['employee_name']; ?>" required>
                            </div>
                          </div>
                        </div>
                        <div class="form-group" id="Employee" class="col-lg-12">
                          <div class="col-lg-6">
                            <label for="department" class="control-label col-lg-2">Month</label>
                            <div class="col-lg-10">
                                <select class="form-control department" name="month" id="month" required>
                                <?php
                                     if( ($this->session->userdata("session_pf_month") )
                                     ){
                                        ?>
                                            <option value="<?php echo $this->session->userdata("session_pf_month"); ?>"><?php echo $this->session->userdata("session_pf_month"); ?></option>
                                        <?php
                                     }
                                ?>
                                    <option value="Jan">January</option>
                                    <option value="Feb">February</option>
                                    <option value="Mar">March</option>
                                    <option value="Apr">April</option>
                                    <option value="May">May</option>
                                    <option value="Jun">June</option>
                                    <option value="Jul">July</option>
                                    <option value="Aug">August</option>
                                    <option value="Sep">September</option>
                                    <option value="Oct">October</option>
                                    <option value="Nov">November</option>
                                    <option value="Dec">December</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <label for="department" class="control-label col-lg-2">Pf Amount</label>
                            <div class="col-lg-10">
                                <input class="form-control department" type="number" name="pf_amount" id="pf_amount" >
                            </div>
                          </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <a class="btn btn-primary col-lg-2" style="float:left" id="prev"> << </a>
                                <a class="btn btn-primary col-lg-2" style="float:right" id="next"> >> </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary col-lg-2" style="margin-left:30%" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
var c=0;
$(document).ready(function(){

    $('#Employee_name_search').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                $.ajax({  
                     url:"<?php echo base_url(); ?>employee/searchemployee/",  
                     method:"POST",  
                     data:{query:query},  
                     success:function(data)  
                     {  console.log(data);
                          $('#employeeList').fadeIn();  
                          $('#employeeList').html(data);  
                     }  
                }); 
           }  
      });  

      $(document).on('click', 'li', function(){  
          var employee_name = $('#Employee_name_search').val($(this).text());
          var date = $("#date").val();
          employee_name = employee_name.val();
           $('#Employee_name_search').val($(this).text());  
        //    $('#employeeList').fadeOut();  
           $('#employeeList').css("display","none");

            $.ajax({  
                     url:"<?php echo base_url(); ?>employee/setemployee/",  
                     method:"POST",  
                     data:{employee_name:employee_name},  
                     success:function(data)  
                     {  
                        // console.log("search_data"+data);
                        $.each($.parseJSON(data), function (index, value) {
                            if(index=='employee_id'){
                                $("#Employee_id").val(value);

                                $.ajax({
                                    type: "post",
                                    url: "<?php echo base_url(); ?>employee/get_employee_dailywork/"+value,
                                    success: function (dailywork_data) {
                                        console.log("data"+dailywork_data);

                                        $.each($.parseJSON(dailywork_data), function (index, value) {
                                            $("#current_month_diamond").val(value[0]['total_diamond']);
                                        });

                                    }
                                });

                                $.ajax({
                            type: "post",
                            url: "<?php echo base_url(); ?>employee/check_diamond_Available/"+date,
                            data:{date:date,Employee_id:value},
                            success: function (data) {
                                console.log("diamondAvailable/Not"+data);
                                $.each($.parseJSON(data), function (index, value) {
                                            //$("#today_diamond").val(value[0]['today_diamond']);  
                                    if(value==0){
                                        //alert("This Employee do not any work for selected Date");
                                        $('#AddOREdit').val('add');
                                        $("#today_diamond").val(0);
                                    }else{
                                        $('#AddOREdit').val('edit');
                                        $.each($.parseJSON(data), function (index, value) {
                                            $("#today_diamond").val(value[0]['today_diamond']);
                                        });
                                    }
                                });
                            }
                });
                            }
                            if(index=='employee_name'){
                                $("#Employee_name").val(value);
                            }
                            
                        });
                        //   $('#employeeList').fadeIn();  
                          $('#employeeList').html(data);  
                     }  
                });  

          
      }); 
      
    $("#next").on("click",function(){
        var PfEmployee_id = $("#PfEmployee_id").val();
        PfEmployee_id=parseInt(PfEmployee_id)+1;
        c = c+1;
        var arrayFromPHP = <?php echo json_encode($Pf_employee); ?>;
        var last_employee = getLastEmployee(arrayFromPHP);
        // console.log("last_employee"+last_employee);
        
        if(!arrayFromPHP[c]){
            $("#PfEmployee_id").val(arrayFromPHP[0]['Pf_employee_Id']);
            $("#Employee_id").val(arrayFromPHP[0]['employee_id']);
            $("#Employee_name").val(arrayFromPHP[0]['employee_name']);
            // var employee_id = arrayFromPHP[0]['employee_id'];
            c=0;
        }else{
            $("#PfEmployee_id").val(arrayFromPHP[c]['Pf_employee_Id']);
            $("#Employee_id").val(arrayFromPHP[c]["employee_id"]);
            $("#Employee_name").val(arrayFromPHP[c]["employee_name"]);
        }

        // $("#PfEmployee_id").val(arrayFromPHP[c]['Pf_employee_Id']);
        // $("#Employee_id").val(arrayFromPHP[c]["employee_id"]);
        // $("#Employee_name").val(arrayFromPHP[c]["employee_name"]);
    });

    $("#prev").on("click",function(){
        var PfEmployee_id = $("#PfEmployee_id").val();
        PfEmployee_id=parseInt(PfEmployee_id)+1;
        c = c-1;
        var arrayFromPHP = <?php echo json_encode($Pf_employee); ?>;
        console.log(arrayFromPHP);
        if(c<0){
            var last_employee = getLastEmployee(arrayFromPHP);
            $("#PfEmployee_id").val(last_employee['Pf_employee_Id']);
            $("#Employee_id").val(last_employee["employee_id"]);
            $("#Employee_name").val(last_employee["employee_name"]);
            c=(arrayFromPHP.length)-1;
        }else{
            $("#PfEmployee_id").val(arrayFromPHP[c]['Pf_employee_Id']);
            $("#Employee_id").val(arrayFromPHP[c]["employee_id"]);
            $("#Employee_name").val(arrayFromPHP[c]["employee_name"]);
        }
    });

    var getLastEmployee =  function(array, n) {
    if (array == null) 
        return void 0;
    if (n == null) 
        return array[array.length - 1];
    return array.slice(Math.max(array.length - n, 0));  
    };

});

$("#month").on("change",function(){
    $.ajax({
                            type: "post",
                            url: "<?php echo base_url(); ?>employee/setGloaballPfMonth/"+this.value,
                            success: function (data) {
                                console.log(data);
                            }
                        });
});
</script>
