<div class="row">

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius" >
        <a href="<?php echo  base_url(); ?>site/department.html">
            <div class="info-box" style="background-color:#E74C3C;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Department</div>
                <div class="title">Manage department</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/employee.html">
            <div class="info-box" style="background-color:#337ab7;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Employee</div>
                <div class="title">Manage employee</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/gaatEmployee.html">
            <div class="info-box" style="background-color:#E74C3C;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Gat Employee</div>
                <div class="title">Manage Gat employee</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/dailywork.html">
            <div class="info-box" style="background-color:#337ab7;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">DailyWork</div>
                <div class="title">Add Employee Daily Work</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/salary.html">
            <div class="info-box" style="background-color:#E74C3C;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Salary</div>
                <div class="title">Manage salary</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/manager_salary.html">
            <div class="info-box" style="background-color:#337ab7;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Manager/Gat Salary</div>
                <div class="title">Manage Manager/Gat salary</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->

    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/report.html">
            <div class="info-box" style="background-color:#E74C3C;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Report</div>
                <div class="title">Manage Employee Report</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/display_otherPayHistory.html">
            <div class="info-box" style="background-color:#337ab7;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Other Pay</div>
                <div class="title">Extra Payment Employee</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 borderRadius">
        <a href="<?php echo  base_url(); ?>site/Pf_employee.html">
            <div class="info-box" style="background-color:#E74C3C;border-radius: 10px;">
                <div class="count" style="font-size: 25px;">Pf Employee</div>
                <div class="title">Manage Pf Employee</div>
            </div><!--/.info-box-->
        </a>
    </div><!--/.col-->
</div>

