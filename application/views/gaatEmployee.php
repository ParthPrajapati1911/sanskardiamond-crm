<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<?php require_once("modal/add_gatEmployee.php"); ?>
<div class="row">
    <div class="col-lg-12 form-group">
        <div class="panel-body col-lg-2">
            <a class="btn btn-primary pull-left" href="" data-toggle="modal" data-target="#myModal" id="add">Add New</a>
        </div>
        
    </div>
</div>

<div class="row">
<?php if($this->session->flashdata('addgaatEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> GaatEmployee Added.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('deletegaatEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> GaatEmployee Deleted.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('editgaatEmployee')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> GaatEmployee Edited.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body table-responsive">
                <table class="table table-responsive table-striped table-advance table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th><i class="icon_profile"></i> Id</th>
                            <th><i class="icon_profile"></i> Name</th>
                            <th><i class="icon_datareport_alt"></i> Designation </th>
                            <th><i class="icon_datareport_alt"></i> City </th>
                            <th><i class="icon_datareport"></i> Rate </th>
                            <th><i class="icon_datareport"></i> Contact No </th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($results as $list){ ?>
                        <tr>
                            <td><?php echo $list->employee_id; ?></td>
                            <td>
                                <?php echo $list->employee_name; ?>
                            </td>
                            <td><?php echo $list->employee_designation; ?></td>
                            <td><?php echo $list->employee_city; ?></td>
                            <td><?php echo $list->rate; ?></td>
                            <td><?php echo $list->employee_phone; ?></td>
                            <td>
                                <a href="<?php echo base_url(); ?>employee/update_gatEmployee/<?php echo $list->employee_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:green;font-size:20px"></i></a>
								|
								<a  href="<?php echo base_url(); ?>employee/delete_gatEmployee/<?php echo $list->employee_id; ?>"><i class="fa fa-trash-o" aria-hidden="true" style="color:red;font-size:20px"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php echo $links; ?></p></div>
    </div>
</div>
<script>
$(document).ready(function(){
     $("select.department").change(function(){
        var department_id = $(this).children("option:selected").val();
        //alert("You have selected the country - " + department_id);
       
        if(department_id){
            
            $.ajax({
                type:'POST',
                url:"<?php echo base_url(); ?>employee/get_employee_data_setTable/"+department_id, 
                
                success:function(html){
                    console.log('SUCCESS: ', html);
                    $('#employee_Data').html(html);
                    $.ajax({
                            type: "post",
                            url: "<?php echo base_url(); ?>department/get_department_rate/"+department_id,
                            success: function (rate) {
                                console.log(rate);

                                $.each($.parseJSON(rate), function (index, value) {
                                    
                                    $("#temp_salary").val(value['department_rate']);

                                });

                            }
                        });
                }
            });
        }
     });
});
</script>
