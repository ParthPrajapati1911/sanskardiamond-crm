<?php 
    require_once ('vendor/autoload.php');
?>
<html>
    <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        #mainTable {
        border-collapse: collapse;
        text-align:center;
        }

        table, th, td {
        border: 0.5px solid black;
        text-align:center
        }
    </style>
    </head>
    <body>
    <table class="table table-striped salary_receipt" id="mainTable" style="width:100%">
      <?php /*echo "<pre>";print_r($employee);exit;*/ for($i=0;$i<count($employee);$i++){
           if($employee[$i]['lastDebit_FixDeposite']==0){
                $baki=0;
            }else{
                $baki=$employee[$i]['lastDebit_FixDeposite'];
            }
            if(empty($employee[$i]['pf'])){
                $employee[$i]['pf'][0]['amount']=0;
            }
            if(!empty($employee[$i]['employee_designation'])){
                $employee_designation = $employee[$i]['employee_designation'];
            }else{
                $employee_designation = $Department_name['department_name'];
            }
            ?>
        <table class="table table-striped" style="width:100%">
           <tr>
              <td>
                    <table class="table table-striped" style="width:100%">
                            <!-- <tr>
                                <td colspan="3">સંસ્કાર ડાયમંડ</td>
                            </tr> -->
                            <tr>
                                <td><strong>નામ : </strong><?php echo convert_name($employee[$i]['employee_name']);?></td>
                                <td><strong>તારીખ : </strong><?php echo date("d-m-Y");?></td>
                            </tr>
                        
                    
                            <tr>
                                <th><?php echo date("d-m-Y", strtotime($str_date));?></th>
                        		<th><?php echo date("d-m-Y", strtotime($ed_date));;?></th>
                            </tr>
                            <tr>
                                <td><strong>વિભાગ : </strong><?php echo convert_name($employee_designation);?></td>
                                <td><strong>ગામ : </strong><?php echo convert_name($employee[$i]['employee_city']);?></td>
                            </tr>
                            <tr>
                                <td><strong>હીરા : </strong><?php echo $employee[$i]['diamond'];?></td>
      <?php if($employee[$i]['pf'][0]['amount']!=0){ ?><td><strong>P.F : </strong><?php echo $employee[$i]['pf'][0]['amount'];?></td><?php }else{?><td></td><?php }?>
                            </tr>
                            <tr>
                                <td><strong>કામ : </strong><?php echo $employee[$i]['salary'];?></td>
        <?php if(empty($employee[$i]['baki'])){?><td></td><?php }else{ ?><td><strong>ટોટલ બાકી : </strong><?php echo $employee[$i]['payment']+$employee[$i]['lastDebit_FixDeposite'];}?></td>
                            </tr>
                            <tr>
                                <td><strong>પગાર : </strong><?php if( (!empty($employee[$i]['lastDebit_FixDeposite'])) || (!empty($employee[$i]['pf'][0])) ){echo $employee[$i]['salary']-$baki-$employee[$i]['pf'][0]['amount'];}else{echo $employee[$i]['salary'];}?></td>
                                <?php if($employee[$i]['lastDebit_FixDeposite']!=0){ ?><td><strong>જ.બાકી.બાદ : </strong><?php $baki=$employee[$i]['lastDebit_FixDeposite']; echo $baki;}else{?></td><?php ?><td></td><?php } ?>
                            </tr>
                            <?php if($employee[$i]['lastDebit_FixDeposite']!=0){?>
                            <tr>
                                <td></td>
                            <?php if(empty($employee[$i]['baki'])){?><td></td><?php }else{ ?> <td><strong>છેલ્લી બાકી : </strong> <?php echo $employee[$i]['payment'];}?></td>
                            </tr>
                            <?php } ?>
                        </table>
                </td>
                <?php if(!empty($employee[$i+1])){
                    
                    if($employee[$i+1]['lastDebit_FixDeposite']==0){
                        $baki=0;
                    }else{
                        $baki=$employee[$i+1]['lastDebit_FixDeposite'];
                    }
                    if(empty($employee[$i+1]['pf'])){
                        $employee[$i+1]['pf'][0]['amount']=0;
                    }
                    if(!empty($employee[$i+1]['employee_designation'])){
                        $employee_designation = $employee[$i+1]['employee_designation'];
                    }else{
                        $employee_designation = $Department_name['department_name'];
                    }
                    ?>
                <td>
                <table class="table table-striped" style="width:100%">
                            <!-- <tr>
                                <td colspan="3">સંસ્કાર ડાયમંડ</td>
                            </tr> -->
                            <tr>
                                <td><strong>નામ : </strong><?php echo convert_name($employee[$i+1]['employee_name']);?></td>
                                <td><strong>તારીખ : </strong><?php echo date("d-m-Y");?></td>
                            </tr>
                        
                    
                            <tr>
                                <th><?php echo date("d-m-Y", strtotime($str_date));?></th>
                        		<th><?php echo date("d-m-Y", strtotime($ed_date));;?></th>
                            </tr>
                            <tr>
                                <td><strong>વિભાગ : </strong><?php echo convert_name($employee_designation);?></td>
                                <td><strong>ગામ : </strong><?php echo convert_name($employee[$i+1]['employee_city']);?></td>
                            </tr>
                            
                            <tr>
                                <td><strong>હીરા : </strong><?php echo $employee[$i+1]['diamond'];?></td>
      <?php if($employee[$i+1]['pf'][0]['amount']!=0){ ?><td><strong>P.F : </strong><?php echo $employee[$i+1]['pf'][0]['amount'];?></td><?php }else{?><td></td><?php }?>
                            </tr>
                            <tr>
                                <td><strong>કામ : </strong><?php echo $employee[$i+1]['salary'];?></td>
                <?php if(empty($employee[$i+1]['baki'])){?><td></td><?php }else{ ?><td><strong>ટોટલ બાકી : </strong> <?php echo $employee[$i+1]['payment']+$employee[$i+1]['lastDebit_FixDeposite'];}?></td>
                            </tr>
                            <tr>
                                <td><strong>પગાર : </strong><?php if( (!empty($employee[$i+1]['lastDebit_FixDeposite'])) || (!empty($employee[$i+1]['pf'][0])) ){echo $employee[$i+1]['salary']-$baki-$employee[$i+1]['pf'][0]['amount'];}else{echo $employee[$i+1]['salary'];}?></td>
                                <?php if($employee[$i+1]['lastDebit_FixDeposite']!=0){ ?><td><strong>જ.બાકી.બાદ : </strong><?php $baki=$employee[$i+1]['lastDebit_FixDeposite']; echo $baki;}else{?></td><?php ?><td></td><?php } ?>
                            </tr>
                            <?php if($employee[$i+1]['lastDebit_FixDeposite']!=0){?>
                            <tr>
                                <td></td>
                            <?php if(empty($employee[$i+1]['baki'])){ ?><td></td><?php }else{ ?><td><strong>છેલ્લી બાકી : </strong> <?php echo $employee[$i+1]['payment'];}?></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </td>
                    <?php }else{
                        break;
                    } ?>
              </tr>
            </table>
      <?php $i=$i+1;} ?>
    </table>
    
    <script type="text/javascript">
        window.onload = function() { window.print(); }
    </script>
    </body>

</html>
<?php

/**For convert english string to gujarati */
use \Statickidz\GoogleTranslate;

function convert_name($name){
    $source = 'en'; //english
    $target = 'gu'; //gujarati
    $trans = new GoogleTranslate();
    $result = $trans->translate($source, $target, $name);
    return $result;
}

?>