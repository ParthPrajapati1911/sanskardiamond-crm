<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<div class="row">
   <?php if($this->session->flashdata('addSalary')){?>
   <div class="alert alert-warning" role="alert">
      <strong style="color:black"> Employee Salary Added.</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <?php }?>
   <?php if($this->session->flashdata('deleteSalary')){?>
   <div class="alert alert-warning" role="alert">
      <strong style="color:black"> Employee Salary Deleted.</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <?php }?>
   <?php if($this->session->flashdata('editSalary')){?>
   <div class="alert alert-warning" role="alert">
      <strong style="color:black"> Employee Salary Edited.</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <?php }?>
   <div class="col-lg-12">
      <section class="panel">
         <div class="panel-body">
            <div class="form">
               <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url(); ?>employee/add_manager_salary.html">
                  <div class="form-group" class="col-lg-12">
                     <label for="type" class="control-label col-lg-1">Department</label>
                     <div class="col-lg-11">
                        <select class="form-control department" name="department" id="department_id" >
                           <?php
                              if($this->session->userdata("session_department_id")){ ?>
                           <!-- <option value=""><?php echo $this->session->userdata("session_department_id");?></option> -->
                           <?php }else{ ?>
                           <option value="">All Selected</option>
                           <?php }
                              ?>
                           <script>
                              var pageLoadTimeDepartmentId = 0; 
                           </script>
                           <?php 
                              if(($this->session->userdata("session_department_id"))
                               && ($this->session->userdata("session_department_name")) 
                               ){
                              ?>
                           <option value="<?php echo $this->session->userdata("session_department_id"); ?>">
                              <?php echo $this->session->userdata("session_department_name"); ?>
                           </option>
                           <script>
                              pageLoadTimeDepartmentId = <?php echo $this->session->userdata   ('session_department_id')?>;
                           </script>
                           <?php }?>
                           <?php $c=0; foreach($department as $data[$c]){
                              $res=array($data[$c]);
                              foreach ($res as $key => $value) {
                                  $result['department_name'] = $value->department_name; 
                                  $result['department_id'] = $value->department_id;
                                  if($this->session->userdata("session_department_id")==$result['department_id']){
                                      continue;
                                  }else{
                                  ?>
                           <option value="<?php echo $result['department_id'] ;?>">
                              <?php echo $result['department_name'] ;?>
                           </option>
                           <?php  } } ?>
                           <?php $c++; } ?>
                        </select>
                     </div>
                  </div>
                  <div class="form-group" id="Employee" class="col-lg-12">
                     <div class="col-lg-6">
                        <label for="department" class="control-label col-lg-2">Search Name </label>
                        <div class="col-lg-10">
                           <input type="text" class="form-control department" placeholder="Search employee name" name="Employee_name_search" id="Employee_name_search">
                           <div id="employeeList"></div>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <label for="department" class="control-label col-lg-2">Employee Name </label>
                        <div class="col-lg-10">
                           <input class="form-control department" type="text" name="Employee_id" id="Employee_id">
                           <input class="form-control department" type="text" name="Employee_name" id="Employee_name" required>
                        </div>
                     </div>
                  </div>
                  <div class="form-group" id="Employee" class="col-lg-12">
                     <div class="col-lg-6">
                        <label for="department" class="control-label col-lg-2">start Date</label>
                        <div class="col-lg-10">
                           <!-- <input class="form-control department" type="date" class="form-control department" name="date" id="start_date" required> -->
                           <?php 
                              if($this->session->userdata("session_start_date")){?>
                           <input class="form-control department" type="date" name="date" id="start_date" value="<?php echo $this->session->userdata("session_start_date");?>" required>
                           <input type="hidden" name="str_date" id="str_date" value="<?php echo $this->session->userdata("session_start_date");?>">
                           <?php }else{?>
                           <input class="form-control department" type="date" name="date" id="start_date" required>
                           <input type="hidden" name="str_date" id="str_date">
                           <?php }
                              ?>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <label for="department" class="control-label col-lg-2">End Date</label>
                        <div class="col-lg-10">
                           <!-- <input class="form-control department" type="date" name="date" id="end_date" required>
                              <input type="hidden" name="ed_date" id="ed_date"> -->
                           <?php 
                              if($this->session->userdata("session_end_date")){?>
                           <input class="form-control department" type="date" name="date" id="end_date" value="<?php echo $this->session->userdata("session_end_date");?>" required>
                           <input type="hidden" name="ed_date" id="ed_date" value="<?php echo $this->session->userdata("session_end_date");?>">
                           <?php }else{?>
                           <input class="form-control department" type="date" name="date" id="end_date" required>
                           <input type="hidden" name="ed_date" id="ed_date">
                           <?php } ?>
                        </div>
                     </div>
                  </div>
                  <div class="form-group" id="Employee" class="col-lg-12">
                     <div class="col-lg-6">
                        <label for="department" class="control-label col-lg-2">Salary </label>
                        <div class="col-lg-10">
                           <input type="number" class="form-control department" name="salary" id="total_salary" required>
                           <input type="hidden" name="temp_salary" id="temp_salary" value="0" required>
                           <input type="hidden" name="Manger_Rate" id="Manger_Rate" required>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <label for="department" class="control-label col-lg-2">Diamond</label>
                        <div class="col-lg-10">
                           <input type="number" class="form-control department" name="current_month_diamond" id="current_month_diamond">
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-lg-offset-2 col-lg-10">
                        <a class="btn btn-primary col-lg-2" style="float:left" id="prev"> << </a>
                        <a class="btn btn-primary col-lg-2" style="float:right" id="next"> >> </a>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-primary col-lg-2" style="margin-left:30%" type="submit">Add</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </section>
   </div>
</div>
<script type="text/javascript">
   var c=0;
   var start_date;
   var end_date;
   $(document).ready(function(){
      
         $('#Employee_name_search').keyup(function(){  
              var query = $(this).val();  
              if(query != '')  
              {  
                   $.ajax({  
                        url:"<?php echo base_url(); ?>employee/searchemployee/",  
                        method:"POST",  
                        data:{query:query},  
                        success:function(data)  
                        {  
                             $('#employeeList').fadeIn();  
                             $('#employeeList').html(data);  
                        }  
                   }); 
              }  
         });  
         $(document).on('click', 'li', function(){  
             var employee_name = $('#Employee_name_search').val($(this).text());
             var date = $("#date").val();
             employee_name = employee_name.val();
              $('#Employee_name_search').val($(this).text());  
           //    $('#employeeList').fadeOut();  
              $('#employeeList').css("display","none");
   
               $.ajax({  
                        url:"<?php echo base_url(); ?>employee/setemployee/",  
                        method:"POST",  
                        data:{employee_name:employee_name},  
                        success:function(data)  
                        {  
                           // console.log("search_data"+data);
                           $.each($.parseJSON(data), function (index, value) {
                               if(index=='employee_id'){
                                   $("#Employee_id").val(value);
   
                                   $.ajax({
                                       type: "post",
                                       url: "<?php echo base_url(); ?>employee/get_employee_dailywork/"+value,
                                       success: function (dailywork_data) {
                                           console.log("data"+dailywork_data);
   
                                           $.each($.parseJSON(dailywork_data), function (index, value) {
                                               $("#current_month_diamond").val(value[0]['total_diamond']);
                                           });
                                                                       
                                       }
                                   });
   
                //                    $.ajax({
                //                type: "post",
                //                url: "<?php echo base_url(); ?>employee/check_diamond_Available/"+date,
                //                data:{date:date,Employee_id:value},
                //                success: function (data) {
                //                    console.log("diamondAvailable/Not"+data);
                //                    $.each($.parseJSON(data), function (index, value) {
                //                                //$("#today_diamond").val(value[0]['today_diamond']);  
                //                        if(value==0){
                //                            //alert("This Employee do not any work for selected Date");
                //                            $('#AddOREdit').val('add');
                //                            $("#today_diamond").val(0);
                //                        }else{
                //                            $('#AddOREdit').val('edit');
                //                            $.each($.parseJSON(data), function (index, value) {
                //                                $("#today_diamond").val(value[0]['today_diamond']);
                //                            });
                //                        }
                //                    });
                //                }
                //    });
                               }
                               if(index=='employee_name'){
                                   $("#Employee_name").val(value);
                               }
                               
                           });
                           //   $('#employeeList').fadeIn();  
                             $('#employeeList').html(data);  
                        }  
                   });  
   
             
         });   
    
   
       $("#next").on("click",function(){
           var department_id = $("select.department").children("option:selected").val();
           var Employee_id = $("#Employee_id").val();
           var date = $("#date").val();
           $('#Employee_name_search').val(''); 
   
           Employee_id=c+1;
           //alert(Employee_id);
           if(department_id){
               $.ajax({
                   type:'POST',
                   url:"<?php echo base_url(); ?>employee/get_employee_data/"+department_id, 
                   
                   success:function(data){
                       console.log(data);
                       $.each($.parseJSON(data), function (index, value) {
                           // $("#Employee_name").val(value[Employee_id]['employee_name']);
                           // $("#Employee_id").val(value[Employee_id]['employee_id']);
                           
                           // var employee_id = value[c]['employee_id'];
   
                           /**For first employee then click prev button then goes to last employee */
                           var last_employee = getLastEmployee(value);
                           console.log("emp_id"+Employee_id);
                           // if(value[Employee_id]['employee_id']==last_employee.employee_id){
                           if(!value[Employee_id]){
                               // console.log("length"+value.length);
                               // console.log("value"+value);
                               // console.log("last employee_name"+last_employee.employee_id)
                               $("#Employee_id").val(value[0]['employee_id']);
                               $("#Employee_name").val(value[0]['employee_name']);
                               var employee_id = value[0]['employee_id'];
                               c=0;
                           }else{
                               $("#Employee_id").val(value[Employee_id]['employee_id']);
                               $("#Employee_name").val(value[Employee_id]['employee_name']);
                               var employee_id = value[Employee_id]['employee_id'];
                           }
   
                           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/get_manager_rate/"+employee_id,
                               success: function (Manager_Salary) {
                                   console.log("Manager_Salary"+Manager_Salary);
   
                                   $.each($.parseJSON(Manager_Salary), function (index, value) {
                                       $("#Manger_Rate").val(value[0]['rate']);
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                                   });
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                               }
                           });
                           var employee_id=$("#Employee_id").val();
                           var start_date=$("#str_date").val();
                           var end_date=$("#ed_date").val();
                           
                           $.ajax({
                                               type: "post",
                                               url: "<?php echo base_url(); ?>employee/get_employee_salary/"+employee_id,
                                               data:{startDate:start_date,endDate:end_date},
                                               success: function (salary) {
                                                   console.log("salary"+salary);
                                                   
                                                   $.each($.parseJSON(salary), function (index, value) {
                                                       // if(value[0]['total_diamond']==null){
                                                       //     alert("Salary Alerady Paid");
                                                       //     return false;
                                                       // }
                                                       if(value[0]['total_diamond']>0){
                                                           $("#current_month_diamond").val(value[0]['total_diamond']);
                                                       }else{
                                                           $("#current_month_diamond").val(0);
                                                       }
                                                   });
                                                   $("#total_salary").val($("#Manger_Rate").val()*$("#current_month_diamond").val());
                                               }
                                   });
   
                       });
                   },
               });
           }
           c++;
       });
   
       $("#prev").on("click",function(){
           var department_id = $("select.department").children("option:selected").val();
           var Employee_id = $("#Employee_id").val();
           var date = $("#date").val();
           $('#Employee_name_search').val(''); 
           Employee_id=c-1;
   
           //alert(Employee_id);
           if(department_id){
               $.ajax({
                   type:'POST',
                   url:"<?php echo base_url(); ?>employee/get_employee_data/"+department_id, 
                   
                   success:function(data){
                       $.each($.parseJSON(data), function (index, value) {
                           // $("#Employee_name").val(value[Employee_id]['employee_name']);
                           // $("#Employee_id").val(value[Employee_id]['employee_id']);
   
                           // var employee_id = value[c]['employee_id'];
                           if(Employee_id<0){
                               // console.log("length"+value.length);
                               // console.log("value"+value);
                               var last_employee = getLastEmployee(value);
                               // console.log("last employee_name"+last_employee.employee_id);
                               $("#Employee_id").val(last_employee.employee_id);
                               $("#Employee_name").val(last_employee.employee_name);
                               var employee_id = last_employee.employee_id;
                               c=(value.length)-1;
                           }else{
                               $("#Employee_id").val(value[Employee_id]['employee_id']);
                               $("#Employee_name").val(value[Employee_id]['employee_name']);
                               var employee_id = value[Employee_id]['employee_id'];
                           }
                           
                           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/get_manager_rate/"+employee_id,
                               success: function (Manager_Salary) {
                                   console.log("Manager_Salary"+Manager_Salary);
   
                                   $.each($.parseJSON(Manager_Salary), function (index, value) {
                                       $("#Manger_Rate").val(value[0]['rate']);
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                                   });
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                               }
                           });
                           var employee_id=$("#Employee_id").val();
                           var start_date=$("#str_date").val();
                           var end_date=$("#ed_date").val();
                           
                           $.ajax({
                                               type: "post",
                                               url: "<?php echo base_url(); ?>employee/get_employee_salary/"+employee_id,
                                               data:{startDate:start_date,endDate:end_date},
                                               success: function (salary) {
                                                   console.log("salary"+salary);
                                                   
                                                   $.each($.parseJSON(salary), function (index, value) {
                                                       // if(value[0]['total_diamond']==null){
                                                       //     alert("Salary Alerady Paid");
                                                       //     return false;
                                                       // }
                                                       if(value[0]['total_diamond']>0){
                                                           $("#current_month_diamond").val(value[0]['total_diamond']);
                                                       }else{
                                                           $("#current_month_diamond").val(0);
                                                       }
                                                   });
                                                   $("#total_salary").val($("#Manger_Rate").val()*$("#current_month_diamond").val());
                                               }
                                   });
   
                       });
                   },
                   
               });
   
           }
           c--;
       });
   
       $("select.department").change(function(){
           var department_id = $(this).children("option:selected").val();
           // alert("You have selected the country - " + department_id);
          
           if(department_id){
               $.ajax({
                   type:'POST',
                   url:"<?php echo base_url(); ?>employee/get_employee_data/"+department_id, 
                   
                   success:function(data){
                       console.log('SUCCESS: ', data);
                       
                       $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>department/get_department_rate/"+department_id,
                               success: function (rate) {
                                //    console.log("rate12"+rate);
   
                                   $.each($.parseJSON(rate), function (index, value) {
                                       
                                       $("#temp_salary").val(value['department_rate']);
   
                                   });
   
                               }
                           });
   
                       
                       
                       $.each($.parseJSON(data), function (index, value) {
                           $("#Employee_name").val(value[c]['employee_name']);
                           $("#Employee_id").val(value[c]['employee_id']);
                           var employee_id = value[c]['employee_id'];
                           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/get_manager_rate/"+employee_id,
                               success: function (Manager_Salary) {
                                   console.log("Manager_Salary"+Manager_Salary);
   
                                   $.each($.parseJSON(Manager_Salary), function (index, value) {
                                       $("#Manger_Rate").val(value[0]['rate']);
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                                   });
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                               }
                           });
                       });
                       
                   },
                   error: function(data) {
                               console.log('ERROR: ', data);
                           },
                   
               }); 
   
           }
       });
       
       $("#start_date").on("change",function(){
           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/setStartDateGlobally/"+this.value,
                               data:{date:this.value},
                                success: function (data) {
                                   console.log("date : "+data);
                               }
           });
   
           $("#str_date").val(this.value);
       });
       $("#end_date").on("change",function(){
           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/setEndDateGlobally/"+this.value,
                               data:{date:this.value},
                                success: function (data) {
                                   console.log("date : "+data);
                               }
           });
   
           $("#ed_date").val(this.value);
           var employee_id=$("#Employee_id").val();
           var start_date=$("#str_date").val();
           var end_date=$("#ed_date").val();
           if($('#str_date').val()==''){
               alert("Please select Start Date");
           }
           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/get_employee_salary/"+employee_id,
                               data:{startDate:start_date,endDate:end_date},
                               success: function (salary) {
                                   console.log("salary"+salary);
                                   
                                   $.each($.parseJSON(salary), function (index, value) {
                                       // if(value[0]['total_diamond']==null){
                                       //     alert("Salary Alerady Paid");
                                       //     return false;
                                       // }
                                       if(value[0]['total_diamond']!=''){
                                           $("#current_month_diamond").val(value[0]['total_diamond']);
                                       }else{
                                           $("#current_month_diamond").val(0);
                                       }
                                   });
                                   $("#total_salary").val($("#Manger_Rate").val()*$("#current_month_diamond").val());
                               }
                   });
   
           //alert($("#Employee_id").val());
       });
       
       var getLastEmployee =  function(array, n) {
     if (array == null) 
       return void 0;
     if (n == null) 
        return array[array.length - 1];
     return array.slice(Math.max(array.length - n, 0));  
     };
   /**For get employee details by department id */
   function setDepartmentBaseEmployee(department_id){
       $.ajax({
                   type:'POST',
                   url:"<?php echo base_url(); ?>employee/get_employee_data/"+department_id, 
                   
                   success:function(data){
                       console.log('SUCCESS: ', data);
                       
                       $.each($.parseJSON(data), function (index, value) {
                    
                           $('#Employee')
                               .append($("<option></option>")
                                           .attr("value",index)
                                           .text(value[0]['employee_name'])); 
                       });
                       
                       $.each($.parseJSON(data), function (index, value) {
                           $("#Employee_name").val(value[c]['employee_name']);
                           $("#Employee_id").val(value[c]['employee_id']);
                           var employee_id = value[c]['employee_id'];
                           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/get_employee_dailywork/"+employee_id,
                               success: function (dailywork_data) {
                                   console.log(dailywork_data);
   
                                   $.each($.parseJSON(dailywork_data), function (index, value) {
                                       $("#current_month_diamond").val(value[0]['total_diamond']);
                                   });
   
                               }
                           });

                           $.ajax({
                               type: "post",
                               url: "<?php echo base_url(); ?>employee/get_manager_rate/"+employee_id,
                               success: function (Manager_Salary) {
                                   console.log("Manager_Salary"+Manager_Salary);
   
                                   $.each($.parseJSON(Manager_Salary), function (index, value) {
                                       $("#Manger_Rate").val(value[0]['rate']);
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                                   });
                                       //$("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                               }
                           });
                        //    alert($("#current_month_diamond").val());
                           $("#total_salary").val($("#Manger_Rate").val()*$("#current_month_diamond").val());
                           // var Employee_id = $("#Employee_id").val();
            //    var date = $("#date").val();
            //    $.ajax({
            //                    type: "post",
            //                    url: "<?php echo base_url(); ?>employee/check_diamond_Available/"+date,
            //                    data:{date:date,Employee_id:employee_id},
            //                    success: function (data) {
            //                        console.log("diamondAvailable/Not"+data);
            //                        $.each($.parseJSON(data), function (index, value) {
            //                                    //$("#today_diamond").val(value[0]['today_diamond']);  
            //                            if(value==0){
            //                                //alert("This Employee do not any work for selected Date");
            //                                $('#AddOREdit').val('add');
            //                                $("#today_diamond").val(0);
            //                            }else{
            //                                $('#AddOREdit').val('edit');
            //                                $.each($.parseJSON(data), function (index, value) {
            //                                    $("#today_diamond").val(value[0]['today_diamond']);
            //                                });
            //                            }
            //                        });
            //                    }
            //        });
                       });
                       
                   },
                   error: function(data) {
                               console.log('ERROR: ', data);
                           },
                   
               });
           }
   
   
           /**For get employee details by department id when page load */
      if(pageLoadTimeDepartmentId){
           setDepartmentBaseEmployee(pageLoadTimeDepartmentId);
           /**End */
       }
   
   });
   
</script>