<?php
//  header('Content-Type: application/vnd.ms-excel');  
//  header('Content-disposition: attachment; filename='.rand().'.xls'); 
    require_once ('vendor/autoload.php');
    $total_baki = 0;
    $total_general_baki_debit = 0;
    $total_pf = 0;
    $total_last_baki = 0;
    $total_pagar = 0;
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- =============================== -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        
        
<!-- =============================== -->

<style>
    table,th,tr,td{
        text-align:center;
    }
    
</style>
    <form action="salary_recipt" method="post">    
        <label>Enter fix deposite to bellow all employee</label><input type="number" class="form-control" id="fix_deposite" name="fix_deposite" value="<?php echo $this->session->userdata('fix_deposite'); ?>">
   
        <br><br>
        <table class="table table-striped" id="datatable" border="1px solid black" style="width:100%">
            <thead>
                <!-- <tr>
                                    <th><?php echo "Manager";?></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <th colspan="2">તારીખ : <?php echo date("d-m-Y");?></th>
                </tr> -->
                <tr>
                    <th style="font-size:14px" >ક્રમ</th>
                    <th style="font-size:14px" >નામ</th>
                    <th style="font-size:14px" >ગામ</th>
                    <th style="font-size:14px" >ટોટલ બાકી</th>
                    <th style="font-size:14px" >જનરલ બાકી બાદ</th>
                    <th style="font-size:14px" >P.F</th>
                    <th style="font-size:14px" >છેલ્લી બાકી</th>
                    <th style="font-size:14px" >પગાર</th>
                    <th style="font-size:14px">સહી</th>
                    <td colspan="2"></td>
                </tr>
            </thead>
            <tbody>
                <?php  $i=1; /*echo "<pre>";print_r($employee);*/ foreach($employee as $data){
                    if(!empty($data['pf'])){
                        $pf = $data['pf'][0]['amount'];
                    }else{
                        $pf = 0;
                    }
                    if(!empty($data['baki'])){
                        if($data['baki'][0]['baki']<$this->session->userdata('fix_deposite'))
                        {
                            $baki = $data['baki'][0]['baki'];
                        }else{
                            $baki = $this->session->userdata('fix_deposite');
                        }
                    }else{
                        $baki = 0;
                    }
                    if(($data['salary']-$pf)<$baki){
                        $salary = ($data['salary']-$pf)-($data['salary']-$pf);
                    }else{
                        $salary = $data['salary']-$baki-$pf;
                    }
                    ?>
                    <tr>
                        <td><?php /*echo $data['employee_id']*/ echo $i; ?></td>
                        <td><?php echo convert_name($data['employee_name']);?></td>
                        <td><?php echo convert_name($data['employee_city']);?></td>
                        <td><?php if(empty($data['baki'][0]['baki']))
                            {
                                $baki1=0;echo 0;
                            }
                            else
                            {
                                $baki1 = $data['baki'][0]['baki'];
                                echo $data['baki'][0]['baki'];
                                $total_baki = $total_baki + $baki1;
                            }?>
                        </td>
                        <td><?php if($salary!=0)
                            {
                                echo $baki;
                                $total_general_baki_debit = $total_general_baki_debit + $baki;
                            }
                            else
                            {
                                echo $data['salary']-$pf;
                                $baki=$data['salary']-$pf;
                                $total_general_baki_debit = $total_general_baki_debit + $baki;
                            } ?>
                        </td>
                        <td><?php
                            echo $pf; 
                            $total_pf = $total_pf + $pf;
                            ?>
                        </td>
                        <td><?php if($salary!=0)
                            {
                                echo $baki1-$baki;
                                $total_last_baki = $total_last_baki + ($baki1-$baki);
                            }
                            else
                            {
                                echo $data['payment']-($data['salary']-$pf);
                                $total_last_baki = $total_last_baki + ($data['payment']-($data['salary']-$pf));
                            }?>
                        </td>
                        <td>
                            <?php if($salary!=0)
                            {
                                echo $salary;
                                $total_pagar = $total_pagar + $salary;
                            }
                            elseif($salary==0)
                            {
                                echo $salary;
                                $total_pagar = $total_pagar + $salary;
                            }
                            else{
                                echo $data['salary'];
                                $total_pagar = $total_pagar + $data['salary'];
                            }?>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                                    <!-- $total_baki = 0;
                            $total_general_baki_debit = 0;
                            $total_pf = 0;
                            $total_last_baki = 0;
                            $total_pagar = 0; -->
                <?php $i++; } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?= $total_baki ?></td>
                    <td><?= $total_general_baki_debit ?></td>
                    <td><?= $total_pf ?></td>
                    <td><?= $total_last_baki ?></td>
                    <td><?= $total_pagar ?></td>
                    <td colspan="2"></td>
                </tr>
            </tfoot>
        </table>
        <button href="" style="float:right" class="btn btn-success col-lg-2">Next</button>
    </form>
        <script>
            $(document).ready(function() {
                
                $("#fix_deposite").keydown(delay(function (e) {
                
                    fix_deposite = $(this).val();
              
                
                    $.ajax({
                        type: "POST",
                        url:"<?php echo base_url(); ?>employee/set_fix_deposite/", 
                        data: {
                            'fix_deposite' : fix_deposite
                        },
                        success: function(data){
                            console.log(data);
                            window.location.reload();
                            $('#fix_deposite').val('0x');
                        }
                    });
               
               },2000));
            function delay(callback, ms) {
            var timer = 0;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                callback.apply(context, args);
                }, ms || 0);
            };
            }
            // $('#salary').DataTable({
            //     "pageLength": 10,
            //         "order": [ 0, "asc" ] ,
            //     dom: 'Bfrtip',
            //     buttons: [
            //         'copy', 'csv', 'excel', 'pdf', 'print'
            //     ],
            //     aoColumnDefs: [
            //         {
            //             bSortable: false,
            //             aTargets: [ -1 ]
            //         }
            //         ],
            // });

                
            
          
        });
</script>

<?php

/**For convert english string to gujarati */
use \Statickidz\GoogleTranslate;

function convert_name($name){
    $source = 'en'; //english
    $target = 'gu'; //gujarati
    $trans = new GoogleTranslate();
    $result = $trans->translate($source, $target, $name);
    return $result;
}

?>
     
    

