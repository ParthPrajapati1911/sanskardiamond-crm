<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>
        <?php echo $title; ?>
    </title>

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo base_url(); ?>css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="<?php echo base_url(); ?>css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" />
    <!-- full calendar css-->
    <link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="<?php echo base_url(); ?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- owl carousel -->
    <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="<?php echo base_url(); ?>css/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/widgets.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/xcharts.min.css" rel=" stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
          <script src="<?php echo base_url(); ?>js/html5shiv.js"></script>
          <script src="<?php echo base_url(); ?>js/respond.min.js"></script>
          <script src="<?php echo base_url(); ?>js/lte-ie7.js"></script>
        <![endif]-->
    <style>
        #sidebar {
            overflow: scroll;
        }
        
        @media screen and (max-width: 600px) {
            .headerText{
                font-size: 15px;
            }
            #sidebar {
                display: none;
            }
            #barIcon {
                display: block;
            }
        }
        
        @media screen and (max-width: 768px) {
            #sidebar {
                display: none;
            }
            #barIcon {
                display: block;
                float:left;
            }
        }
        @media screen and (min-width: 769px) {
            #barIcon {
                display: none;
            }
            #sidebar{
                display:block;
            }
        }

        .dropbtn {
            background-color: #394a59;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            min-width: 160px;
            text-align: left;
            cursor: pointer;
        }
        /* The container <div> - needed to position the dropdown content */
        
        .dropdown {
            position: relative;
            display: inline-block;
        }
        /* Dropdown Content (Hidden by Default) */
        
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #394a59;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }
        /* Links inside the dropdown */
        
        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        /* Change color of dropdown links on hover */
        
        .dropdown-content a:hover {
            background-color: #f1f1f1
        }
        /* Show the dropdown menu on hover */
        
        .dropdown:hover .dropdown-content {
            display: block;
        }
        /* Change the background color of the dropdown button when the dropdown content is shown */
        
        .dropdown:hover .dropbtn {
            background-color: #2e3b46;
        }
    </style>

</head>

<body>
    <section id="container">
        <!-- container section start -->
        <header class="header dark-bg" style="background-color:#337ab7;">
        <div class="col-sm-12 col-lg-12 col-xs-12">
            <!-- <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
            </div> -->
            <div class="col-sm-6 col-lg-10 col-xs-12">
                <!--logo start-->
                <a href="<?php echo base_url(); ?>site" class="logo col-sm-3 headerText" style="color:white">Sanskar <span class="" style="color:white">Diamond</span></a>
                <!--logo end-->
            </div>

            <!-- <div class="col-sm-6"> -->
                <ul class="nav pull-right top-menu col-sm-6 col-lg-2 col-xs-12" style="margin-top:10px">
                    <li class="dropdown col-sm-4 col-lg-4 col-xs-6" style="float:left" id="barIcon">
                        <a href="javascript:void(0);" class="icon" onclick="showNavbar()">
                            <i class="fa fa-bars" style="font-size: 30px;color: white;"></i>
                        </a>
                    </li>

                    <!-- <li class="dropdown col-sm-4 col-lg-4 col-xs-4">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">>
                                    </span>
                            <span class="username">Language</span>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="">English</a>
                            </li>
                            <li>
                                <a href="">Gujarati</a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- </ul> -->

                    <!-- <ul class="nav pull-right top-menu" style="margin-top:5px"> -->
                    <li class="dropdown nav pull-right top-menu col-sm-4 col-lg-8 col-xs-6">
                        <a data-toggle="dropdown col-sm-4 col-lg-6 col-xs-4" class="dropdown-toggle" href="#">
                            <span class="profile-ava col-sm-4 col-lg-6 col-xs-4">
                                        <img alt="avatar" src="<?php echo base_url(); ?>img/admin1.png" height="40px" width="40px">
                                    </span>
                            <span class="username col-sm-4 col-lg-6 col-xs-4"><?php echo $this->session->userdata('username'); ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout col-sm-4 col-lg-6 col-xs-4">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="<?php echo base_url(); ?>site/profile.html"><i class="icon_profile"></i>Profile</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>site/logout.html"><i class="icon_key_alt"></i>Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            <!-- </div> -->
        <div>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu">
                    <li class="<?php if($title=='Home'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site">
                            <i class="icon_house_alt"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Department'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/department.html">
                            <i class="icon_id"></i>
                            <span>Department</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Employee'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/employee.html">
                            <i class="fa fa-user"></i>
                            <span>Employee</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Gaat Employee'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/gaatEmployee.html">
                            <i class="fa fa-user"></i>
                            <span>Gaat Employee</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Manager'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/manager.html">
                            <i class="fa fa-user"></i>
                            <span>Manager</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Dailywork'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/dailyWork.html">
                            <i class="icon_cog"></i>
                            <span>DailyWork</span>
                        </a>
                    </li>
                    <!-- <li class="<?php if($title=='Dailywork'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/gat_dailyWork.html">
                            <i class="icon_cog"></i>
                            <span>Gat DailyWork</span>
                        </a>
                    </li> -->
                    <!-- <li class="<?php if($title=='Salary'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/salary.html">
                            <i class="fa fa-money"></i>
                            <span>Salary</span>
                        </a>
                    </li> -->
                    <!-- <div class="dropdown-menu btn-group" style="background-color:#d0d8df">

                        <a  class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Salary
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo base_url(); ?>site/salary.html" class="dropdown-item" style="background-color:white;color:black">Employee Salary</a></li>
                            <li><a href="<?php echo base_url(); ?>site/manager_salary.html" class="dropdown-item" style="background-color:white;color:black">Manager/Gat Salary</a></li>
                        </ul>

                    </div> -->
                    <!-- <li><a href="<?php echo base_url(); ?>site/gat_salary.html" class="dropdown-item" style="background-color:white;color:black">Gat Employee Salary</a></li> -->
                    <li class="<?php if($title=='Salary'){ echo 'active';}?>" style="border-bottom: 1px solid white">
                        <div class="dropdown">
                            <button class="dropbtn">

                            <i class="fa fa-money" aria-hidden="true" style="margin-left: -6px;padding-right: 9px;"></i>
                                <span>Salary</span></button>

                            <div class="dropdown-content">
                                <a href="<?php echo base_url(); ?>site/salary.html" class="dropdown-item" style="background-color:white;color:black">Employee Salary</a>
                                <a href="<?php echo base_url(); ?>site/manager_salary.html" class="dropdown-item" style="background-color:white;color:black">Manager/Gat Salary</a>
                            </div>
                        </div>
                        <!-- <a href="<?php echo base_url(); ?>site/dailyWork.html">
                                    <i class="icon_cog"></i>
                                    <span>DailyWork</span>
                                </a> -->
                    </li>
                    <!-- <li class="<?php if($title=='Manager Salary'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/manager_salary.html">
                            <i class="fa fa-money"></i>
                            <span>Manager Salary</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Manager Salary'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/gat_salary.html">
                            <i class="fa fa-money"></i>
                            <span>Gat Salary</span>
                        </a>
                    </li> -->
                    <li class="<?php if($title=='Employee Salary Report'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/report.html">
                            <i class="icon_clipboard"></i>
                            <span>Report</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='PF Employee'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/Pf_employee.html">
                            <i class="fa fa-user"></i>
                            <span>PF Employee</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Extra Payment'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/display_otherPayHistory.html">
                            <i class="fa fa-money"></i>
                            <span>Other Payment</span>
                        </a>
                    </li>
                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">

                <script>
                    function showNavbar() {
                        var x = document.getElementById("sidebar");
                        if (x.style.display === "block") {
                            x.style.display = "none";
                        } else {
                            x.style.display = "block";
                        }
                    }
                </script>