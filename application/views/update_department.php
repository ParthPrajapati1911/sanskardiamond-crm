<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="form">
                    <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url(); ?>department/update_department_commit.html">
                        <div class="form-group">
                            <label for="name" class="control-label col-lg-2">Department Name</label>
                            <div class="col-lg-10">
                                <input name="id" type="hidden" value="<?php echo $result->department_id; ?>" />
                                <input class="form-control" name="department_name" type="text" value="<?php echo $result->department_name; ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="department" class="control-label col-lg-2">Rate</label>
                            <div class="col-lg-10">
                                <input class="form-control" type="rate" name="department_rate" value="<?php echo $result->department_rate; ?>"/>
                            </div>
                        </div>
                     
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
