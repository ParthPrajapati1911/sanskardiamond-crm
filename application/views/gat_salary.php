<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<div class="row">
<?php if($this->session->flashdata('addDailywork')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Dailywork Added.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('deleteDailywork')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Dailywork Deleted.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
<?php if($this->session->flashdata('editDailywork')){?>
    <div class="alert alert-warning" role="alert">
    <strong style="color:black"> Employee Dailywork Edited.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
<?php }?>
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="form">
                    <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url(); ?>employee/add_gat_salary.html">
                        
                        

                        <div class="form-group">
                            <label for="type" class="control-label col-lg-1">Gat Employee</label>
                            <div class="col-lg-11">
                                <select class="form-control" id="Employee" name="Employee_id">
                                <option value="">All Selected</option>
                                <?php $c=0; foreach($results as $data[$c]){
                                                $res=array($data[$c]);
                                                foreach ($res as $key => $value) {
                                                    $result['employee_name'] = $value->employee_name; 
                                                    $result['employee_id'] = $value->employee_id;
                                                    ?>
                                        <option value="<?php echo $result['employee_id'] ;?>"><?php echo $result['employee_name'] ;}?></option>

                                    <?php $c++; } ?>
                                </select>
                            </div>
                        </div>
 
                        <!-- <div class="form-group">
                            <label for="department" class="control-label col-lg-1">Search Name </label>
                            <div class="col-lg-11">
                            
                            <input type="text" class="form-control department" placeholder="Search employee name" name="Employee_name_search" id="Employee_name_search">
                            <div id="employeeList"></div>  
                            </div>
                        </div> -->

                        <!-- <div class="form-group" id="Employee" class="col-lg-12">
                          <div class="col-lg-6">
                            <label for="department" class="control-label col-lg-2">Employee Name </label>
                            <div class="col-lg-10">
                                <input class="form-control department" type="text" name="Employee_id" id="Employee_id">
                                <input class="form-control department" type="text" name="Employee_name" id="Employee_name" required>
                            </div>
                          </div> -->
                          <div class="form-group" id="Employee" class="col-lg-12">
                          <div class="col-lg-6">
                            <label for="department" class="control-label col-lg-2">start Date</label>
                            <div class="col-lg-10">
                                <input class="form-control department" type="date" class="form-control department" name="date" id="start_date" required>
                                <input type="text" name="str_date" id="str_date">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <label for="department" class="control-label col-lg-2">End Date</label>
                            <div class="col-lg-10">
                                <input class="form-control department" type="date" name="date" id="end_date" required>
                                <input type="text" name="ed_date" id="ed_date">
                            </div>
                          </div>
                        
                        </div>
                        <div class="form-group" id="Employee" class="col-lg-12">
                         <div class="col-lg-4">
                            <label for="department" class="control-label col-lg-2">Department</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control department" name="dept" id="dept">
                                <input type="text" class="form-control department" name="dept_temp[]" id="dept_temp">
                            </div>
                          </div>
                         <div class="col-lg-4">
                            <label for="department" class="control-label col-lg-2">Diamond</label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control department" name="current_month_diamond[]" id="current_month_diamond">
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <label for="department" class="control-label col-lg-2">Salary </label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control department" name="salary[]" id="salary1" required>
                                <input type="text" name="temp_salary" id="temp_salary" >
                            </div>
                          </div>
                        </div> 
                        <div class="form-group" id="other_dept_work" class="col-lg-12">
                        </div>

                            <!-- <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <a class="btn btn-primary col-lg-2" style="float:left" id="prev"> << </a>
                                    <a class="btn btn-primary col-lg-2" style="float:right" id="next"> >> </a>
                                </div>
                            </div> -->

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary col-lg-2" style="margin-left:30%" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
var c=0;
$(document).ready(function(){

    $('#Employee_name_search').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                $.ajax({  
                     url:"<?php echo base_url(); ?>employee/searchemployee/",  
                     method:"POST",  
                     data:{query:query},  
                     success:function(data)  
                     {  console.log(data);
                          $('#employeeList').fadeIn();  
                          $('#employeeList').html(data);  
                     }  
                }); 
           }  
      });  

      $(document).on('click', 'li', function(){  
          var employee_name = $('#Employee_name_search').val($(this).text());
          employee_name = employee_name.val();
           $('#Employee_name_search').val($(this).text());  
           $('#employeeList').fadeOut();  

           $.ajax({  
                     url:"<?php echo base_url(); ?>employee/setemployee/",  
                     method:"POST",  
                     data:{employee_name:employee_name},  
                     success:function(data)  
                     {  
                         
                          $('#employeeList').fadeIn();  
                          $('#employeeList').html(data);  
                     }  
                });  

          
      });  
      
    $("#start_date").on("change",function(){
        $("#str_date").val(this.value);
    });
    $("#end_date").on("change",function(){
        $("#ed_date").val(this.value);
        if($('#str_date').val()==''){
            alert("Please select Start Date");
        }
        
        var start_date=$("#str_date").val();
        var end_date=$("#ed_date").val();
       
        var Employee_id = $("#Employee").val();
        //var Department_id = $("#department").val();
        //alert(start_date+"dd"+"dd"+end_date+"dd"+Employee_id+"dd"+Department_id);
        $.ajax({
                            type: "post",
                            url: "<?php echo base_url(); ?>employee/gat_employee_salary/",
                            data:{startDate:start_date,endDate:end_date,Employee_id:Employee_id},
                            success: function (salary) {
                                console.log("salary"+salary);
                                //var i=0;
                                //alert(salary.length);
                                //alert(Object.keys(salary).length); 
                                var i=1;
                               
                                $.each($.parseJSON(salary), function (index, value) {
                                    
                                   //alert(value);
                                    //alert("in"+i);
                                    // if(value[0]['total_diamond']==null){
                                    //     alert("Salary Alerady Paid");
                                    //     return false;
                                    // }
                                    
                                    $("#dept_temp").val(value[0]['department_id']);
                                    //$("#dept_temp"+i).val(value[1]['department_id']);
                                    $("#dept").val(value[0]['department_name']);
                                    //$("#dept"+i).val(value[1]['department_name']);
                                    $("#current_month_diamond").val(value[0]['total_diamond']);
                                    //$("#current_month_diamond"+i).val(value[1]['total_diamond']);
                                    $("#salary1").val(value[0]['total_diamond']*value[0]['department_rate']);
                                    //$("#salary1"+i).val(value[1]['total_diamond']*value[1]['department_rate']);
                                    //console.log("data"+value[1]['department_id']);
                                    if(value[1]['department_id']!='undefined'){
                                        var val = ' <div class="col-lg-4"><label for="department" class="control-label col-lg-2">Department</label><div class="col-lg-10"><input type="text" class="form-control department" name="dept[]" id="dept'+i+'"><input type="text" class="form-control department" name="dept_temp[]" id="dept_temp'+i+'"></div></div><div class="col-lg-4"><label for="department" class="control-label col-lg-2">Diamond</label><div class="col-lg-10"><input type="number" class="form-control department" name="current_month_diamond[]" id="current_month_diamond'+i+'"></div></div><div class="col-lg-4"><label for="department" class="control-label col-lg-2">Salary </label><div class="col-lg-10"><input type="number" class="form-control department" name="salary[]" id="salary1'+i+'" required><input type="hidden" name="temp_salary" id="temp_salary" required></div></div>';
                                        //alert(value[0]['department_id']);alert(value[1]['department_id']);
                                        $('#other_dept_work').html(val);

                                        $("#dept_temp"+i).val(value[1]['department_id']);
                                        $("#dept"+i).val(value[1]['department_name']);
                                        $("#current_month_diamond"+i).val(value[1]['total_diamond']);
                                        $("#salary1"+i).val(value[1]['total_diamond']*value[1]['department_rate']);

                                    }else{
                                        alert("else");
                                    }
                                    i++;
                                });
                                $("#salary").val($("#temp_salary").val()*$("#current_month_diamond").val());
                            }
                });

        //alert($("#Employee_id").val());
    });

    // $("select.department").change(function(){
    //     var department_id = $(this).children("option:selected").val();
    //     //alert("You have selected the country - " + department_id);
    //     if(department_id){
    //         $.ajax({
    //             type:'POST',
    //             url:"<?php echo base_url(); ?>employee/get_employee_data/"+department_id, 
                
    //             success:function(data){
    //                 console.log('SUCCESS: ', data);
                    
    //                 $.each($.parseJSON(data), function (index, value) {
                 
    //                     $('#Employee')
    //                         .append($("<option></option>")
    //                                     .attr("value",index)
    //                                     .text(value[0]['employee_name'])); 
    //                 });
                    
    //                 $.each($.parseJSON(data), function (index, value) {
    //                     $("#Employee_name").val(value[c]['employee_name']);
    //                     $("#Employee_id").val(value[c]['employee_id']);
    //                     var employee_id = value[c]['employee_id'];
    //                     $.ajax({
    //                         type: "post",
    //                         url: "<?php echo base_url(); ?>employee/get_employee_dailywork/"+employee_id,
    //                         success: function (dailywork_data) {
    //                             console.log(dailywork_data);

    //                             $.each($.parseJSON(dailywork_data), function (index, value) {
    //                                 $("#current_month_diamond").val(value[0]['total_diamond']);
    //                             });

    //                         }
    //                     });
    //                 });
                    
    //             },
    //             error: function(data) {
    //                         console.log('ERROR: ', data);
    //                     },
                
    //         }); 

    //     }
    // });
});

</script>
