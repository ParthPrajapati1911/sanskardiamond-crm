<?php
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

    function __construct(){

        parent::__construct();

        $flag = $this->session->userdata('flag');

        if($flag == NULL){

            redirect('admin','refresh');
        }
    }

    public function logout(){

        $this->session->unset_userdata('flag');
        $this->session->unset_userdata('username');
        $data['message'] = 'You are successfully logged out';
        $this->session->set_userdata($data);

        redirect('admin','refresh');
    }

    public function index(){

        $data = array();
        $data['title'] = "Home";
        $data['heading'] = "Main Menu";
        $data['content'] = $this->load->view('home',$data,true);
        $this->load->view('master',$data);
    }

    public function employee(){

        $data = array();
        $data["title"] = "Employee";
        $data["heading"] = "Employee Details";
        $data["base_url"] = base_url() . "site/employee";
        $data["total_rows"] = $this->employee_model->record_count();
        $data["per_page"] = 500;
        $data["uri_segment"] = 3;

        // $this->pagination->initialize($data);
        // $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        // $data["results"] = $this->employee_model->fetch_employee($data["per_page"], $page);
        $data["results"] = $this->employee_model->fetch_employee();
        $i = 0;
        foreach($data["results"] as $employee){
            $department = $this->department_model->fetch_department_by_id($employee->employee_department);
            $data["results"][$i]->employee_department = $department->department_name;
            $i = $i+1;
        }
        $data["department"] = $this->department_model->fetch_department();
        // $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('employee',$data,true);
        $this->load->view('master',$data);
    }
    
    public function Pf_employee(){

        $data = array();
        $data["title"] = "PF Employee";
        $data["heading"] = "PF Employee Details";
        $data["base_url"] = base_url() . "site/pf_employee";
        $data["total_rows"] = $this->employee_model->Pf_employee_record_count();
        $data["per_page"] = 50;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["results"] = $this->employee_model->fetch_Pf_employee($data["per_page"], $page);
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $i = 0;
        if($data["results"]){
            foreach($data["results"] as $employee){
                $department = $this->department_model->fetch_department_by_id($employee->employee_department);
                $data["results"][$i]->employee_department = $department->department_name;
                $i = $i+1;
            }
        }
        
        $data["department"] = $this->department_model->fetch_department();
        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('pf_employee',$data,true);
        $this->load->view('master',$data);
    }
    public function add_Pf_employee(){
        $data = array();
        $data['title'] = "Add Employee Pf";
        $data['heading'] = "Add Employee Pf";
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;
        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["Pf_employee"] = $this->employee_model->fetch_Pf_employeeNew($data["per_page"], $page);
        $data['content'] = $this->load->view('add_Pf_employee',$data,true);
        $this->load->view('master',$data);
    }
    public function manager(){

        $data = array();
        $data["title"] = "Manager";
        $data["heading"] = "Manager Details";
        $data["base_url"] = base_url() . "site/manager";
        $data["total_rows"] = $this->employee_model->manager_record_count();
        $data["per_page"] = 50;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["results"] = $this->employee_model->fetch_manager($data["per_page"], $page);
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["department"] = $this->department_model->fetch_department();
        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('manager',$data,true);
        $this->load->view('master',$data);
    }

    public function gaatEmployee(){

        $data = array();
        $data["title"] = "Gaat Employee";
        $data["heading"] = "Gaat Employee Details";
        $data["base_url"] = base_url() . "site/gaatEmployee";
        $data["total_rows"] = $this->employee_model->gaatEmployee_record_count();
        $data["per_page"] = 50;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["results"] = $this->employee_model->fetch_gatEmployee($data["per_page"], $page);
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('gaatEmployee',$data,true);
        $this->load->view('master',$data);
    }

    public function department(){
        $data = array();
        $data["title"] = "Department";
        $data["heading"] = "Department Details";
        $data["base_url"] = base_url() . "site/department";
        $data["total_rows"] = $this->department_model->record_count();
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["results"] = $this->department_model->fetch_department();

        // $data["results"] = $this->department_model->fetch_department($data["per_page"], $page);

        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('department',$data,true);
        $this->load->view('master',$data);
    }

    public function salary(){

        $data = array();
        $data["title"] = "Salary";
        $data["heading"] = "Salary Details";
        $data["base_url"] = base_url() . "site/salary";
        $data["total_rows"] = $this->salary_model->record_count();
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["results"] = $this->salary_model->fetch_salary($data["per_page"], $page);
        // $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('salary',$data,true);
        $this->load->view('master',$data);
    }
    
    public function gat_salary(){

        $data = array();
        $data["title"] = "Gat Salary";
        $data["heading"] = "Gat Salary Details";
        $data["base_url"] = base_url() . "site/gat_salary";
        $data["total_rows"] = $this->salary_model->record_count();
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["results"] = $this->employee_model->fetch_gat_emp();
        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('gat_salary',$data,true);
        $this->load->view('master',$data);
    }

    public function manager_salary(){

        $data = array();
        $data["title"] = "Salary";
        $data["heading"] = "Manager Salary Details";
        $data["base_url"] = base_url() . "site/manager_salary";
        $data["total_rows"] = $this->salary_model->record_count();
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["results"] = $this->salary_model->fetch_salary($data["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('manager_salary',$data,true);
        $this->load->view('master',$data);
    }
    public function profile(){

        $data = array();
        $data['title'] = "Profile";
        $data['heading'] = "Admin Details";
        $data['content'] = $this->load->view('profile',$data,true);
        $this->load->view('master',$data);
    }

    public function dailywork(){
        $data = array();
        $data['title'] = "Dailywork";
        $data['heading'] = "Employee DailyWork";
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;
        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data['content'] = $this->load->view('dailywork',$data,true);
        $this->load->view('master',$data);
    }

    public function gat_dailyWork(){
        $data = array();
        $data['title'] = "Gat Dailywork";
        $data['heading'] = "Gat Employee DailyWork";
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;
        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->employee_model->fetch_gatEmployee($data["per_page"], $page);
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data['content'] = $this->load->view('gat_dailywork',$data,true);
        $this->load->view('master',$data);
    }
    public function create(){

        $data = array();
        $data['title'] = "Create Admin";
        $data['heading'] = "Create New Admin";
        $data['content'] = $this->load->view('create',$data,true);
        $this->load->view('master',$data);
    }

    public function create_admin(){

        $data = array();
        $data['admin_username'] = $this->input->post('username',true);
        $data['admin_password'] = hash("SHA512",$this->input->post('password',true));
        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $data['admin_salt'] = password_hash("rasmuslerdorf", PASSWORD_BCRYPT, $options);
        $this->admin_model->save_admin($data);

        redirect('site');
    }

    public function reset(){

        $data = array();
        $data['title'] = "Reset Password";
        $data['heading'] = "Reset Admin Password";
        $data['content'] = $this->load->view('reset',$data,true);
        $this->load->view('master',$data);
    }

    public function reset_password(){

        $data = array();
        $admin_id = $this->session->userdata('flag');
        $data['admin_password'] = hash("SHA512",$this->input->post('password',true));
        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $data['admin_salt'] = password_hash("rasmuslerdorf", PASSWORD_BCRYPT, $options);
        $this->admin_model->update_password_by_id($admin_id,$data);

        redirect('site');
    }

    public function search(){

        $term = $this->input->get('search',true);
        $this->employee_model->search_employee($term);
    }
    //FOR MANAGE EMPLOYEE REPORT LIKE EMPLOYEE SALARY RECIEPT
    public function report(){
        $data = array();
        $data['title'] = "Employee Salary Report";
        $data['heading'] = "Employee Salary Report";
        $data["per_page"] = 100;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data['content'] = $this->load->view('salary_report',$data,true);
        $this->load->view('master',$data);
    }
    public function display_otherPayHistory(){
        $data = array();
        $data["title"] = "Extra Pay";
        $data["heading"] = "Employee Extra Pay History";
        $data["base_url"] = base_url() . "site/display_otherPayHistory";
        $data["result"] = $this->employee_model->Employee_ExtraPay_record_count();
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        // echo "<pre>";print_r($data);exit;
        $i = 0;
        foreach($data["result"] as $employee){
            $department = $this->department_model->fetch_department_by_id($employee['employee_department']);
            $data["result"][$i]['employee_department'] = $department->department_name;
            $i = $i+1;
        }
        $data["content"] = $this->load->view('ExtraPayHistory',$data,true);
        $this->load->view('master',$data);
    }
    public function extra_pay(){
        $data = array();
        $data["title"] = "Extra Payment";
        $data["heading"] = "Extra Payment for Employee";
        $data["base_url"] = base_url() . "site/department";
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["results"] = $this->salary_model->fetch_salary($data["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $data["content"] = $this->load->view('extra_payment',$data,true);
        $this->load->view('master',$data);
    }
}