<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends CI_Controller {

    function __construct(){

        parent::__construct();
        $this->load->library('session');
    }

    public function view_employee($employee_id){

        $data = array();
        $data['title'] = "View Employee";
        $data['heading'] = "View Employee Details";
        $data['result'] = $this->employee_model->fetch_employee_by_id($employee_id);
        $data['content'] = $this->load->view('view_employee',$data,true);
        $this->load->view('master',$data);
    }

    public function update_department($department_id){

        $data = array();
        $data['title'] = "Update Department";
        $data['heading'] = "Update Department Details";
        $data['result'] = $this->department_model->fetch_department_by_id($department_id);
        $data['content'] = $this->load->view('update_department',$data,true);
        $this->load->view('master',$data);
    }

    public function update_department_commit(){

        $data = array();
        $department_id = $this->input->post('id',true);
        $data['department_name'] = $this->input->post('department_name',true);
        $data['department_rate'] = $this->input->post('department_rate',true);
        
        $this->department_model->update_department_by_id($department_id,$data);
        $this->session->set_flashdata('editDepartment','editDepartment');
        redirect('site/department');
    }

    public function add_department(){

        $data = array();
      
        $data['department_name'] = $this->input->post('department_name',true);
        $data['department_rate'] = $this->input->post('rate',true);
        $this->department_model->save_department($data);
        $this->session->set_flashdata('addDepartment','addDepartment');
        redirect('site/department');
    }

    //move to add employee page 
    public function add_employee_page(){
        $data = array();
        $data['title'] = "Add Employee";
        $data['heading'] = "Add Employee Details";
        $data['content'] = $this->load->view('modal/add_employee');
        $this->load->view('master',$data);
    }
    public function delete_department(){
 
        if(isset($_POST["departmentId"]))  
        {  
            $this->department_model->erase_department($_POST["departmentId"]);
            // $this->session->set_flashdata('deleteDepartment','deleteaddDepartment');
            echo 1;
        }
        
        // return 1;
        // redirect('site/department');
    }
    public function get_department_rate($department_id){
        $data['rate'] = $this->department_model->get_department_rate($department_id);
        print_r(json_encode($data));
    }
}