<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends CI_Controller {

    function __construct(){

        parent::__construct();
        $this->load->library('session');
    }

    public function view_employee($employee_id){

        $data = array();
        $data['title'] = "View Employee";
        $data['heading'] = "View Employee Details";
        $data['result'] = $this->employee_model->fetch_employee_by_id($employee_id);
        $data['content'] = $this->load->view('view_employee',$data,true);
        $this->load->view('master',$data);
    }
    
    public function update_employee($employee_id){
        $data = array();
        $data['title'] = "Update Employee";
        $data['heading'] = "Update Employee Details";
        $data['result'] = $this->employee_model->fetch_employee_by_id($employee_id);    
        $data["per_page"] = 100;
        $data["uri_segment"] = 3;

        $this->pagination->initialize($data);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data["department"] = $this->department_model->fetch_department();
        //print_r($data["department"]);exit;
        $data['content'] = $this->load->view('update_employee',$data,true);
        $this->load->view('master',$data);
    }
    public function updatePf_employee($employee_id){
        $data = array();
        $data['title'] = "Update Pf Employee";
        $data['heading'] = "Update Pf Employee Details";
        $data['result'] = get_object_vars($this->employee_model->fetch_Pfemployee_by_id($employee_id));    
        //print_r($data);exit;
        $data["department"] = $this->department_model->fetch_departmentNew();
        $data['content'] = $this->load->view('update_pfemployee',$data,true);
        $this->load->view('master',$data);
    }
    public function update_manager($manager_id){
        $data = array();
        $data['title'] = "Update Manager";
        $data['heading'] = "Update Manager Details";
        $data['result'] = $this->employee_model->fetch_manager_by_id($manager_id);
        $data['content'] = $this->load->view('update_manager',$data,true);
        $this->load->view('master',$data);
    }
    public function save_PfemployeeData(){
        
        $data = array();
        $employeePfId = $this->input->post('employeePfId',true);
        
        $data['Pf_no'] = $this->input->post('pf_no',true);
        $this->employee_model->update_pfemployee_by_id($employeePfId,$data);
        $this->session->set_flashdata("editPfEmployee","editPfEmployee");
        redirect('site/Pf_employee');
    }
    public function update_gatEmployee($employee_id){
        $data = array();
        $data['title'] = "Update GatEmployee";
        $data['heading'] = "Update GatEmployee Details";
        $data['result'] = $this->employee_model->fetch_GatEmployee_by_id($employee_id);
        $data['content'] = $this->load->view('update_gatEmployee',$data,true);
        $this->load->view('master',$data);
    }

    public function update_employee_commit(){

        $data = array();
        $employee_id = $this->input->post('id',true);
        
        $data['employee_name'] = $this->input->post('name',true);
        $data['employee_department'] = $this->input->post('department',true);
        $data['employee_designation'] = $this->input->post('designation',true);
        $data['employee_phone'] = $this->input->post('phone',true);
        $data['employee_city'] = $this->input->post('address',true);
        $this->employee_model->update_employee_by_id($employee_id,$data);
        $this->session->set_flashdata("editEmployee","editEmployee");
        redirect('site/employee');
    }
    public function get_manager_rate($employee_id){
        $data['result'] = $this->employee_model->get_manager_rate($employee_id);
        print_r(json_encode($data));
    }
    public function get_gatEmployee_rate($employee_id){
        $data['result'] = $this->employee_model->get_gatEmployee_rate($employee_id);
        print_r(json_encode($data));
    }
    public function update_manager_commit(){

        $data = array();
        $manager_id = $this->input->post('id',true);
        
        $data['employee_name'] = $this->input->post('name',true);
        $data['rate'] = $this->input->post('rate',true);
        $data['employee_designation'] = $this->input->post('designation',true);
        $data['employee_phone'] = $this->input->post('phone',true);
        $data['employee_city'] = $this->input->post('address',true);
        $this->employee_model->update_manager_by_id($manager_id,$data);
        $this->session->set_flashdata("editManager","editManager");
        redirect('site/manager');
    }
    public function update_gatEmployee_commit(){

        $data = array();
        $employee_id = $this->input->post('id',true);
        
        $data['employee_name'] = $this->input->post('name',true);
        $data['rate'] = $this->input->post('rate',true);
        $data['employee_designation'] = $this->input->post('designation',true);
        $data['employee_phone'] = $this->input->post('phone',true);
        $data['employee_city'] = $this->input->post('address',true);
        $this->employee_model->update_gaatEmployee_by_id($employee_id,$data);
        $this->session->set_flashdata("editgaatEmployee","editManager");
        redirect('site/gaatEmployee');
    }

    public function add_employee(){

        $data = array();
        $data['employee_name'] = $this->input->post('name',true);
        $data['employee_department'] = $this->input->post('department',true);
        $data['employee_phone'] = $this->input->post('phone',true);
        $data['employee_city'] = $this->input->post('city',true);
        $password = $this->input->post('password',true);
        $data['password'] = md5($password);
        $this->employee_model->save_employee($data);
        $this->session->set_flashdata("addEmployee","addEmployee");
        redirect('site/employee');
    }
    public function add_employee_Pf(){
        $data = array();
        $data['employee_id'] = $this->input->post('Employee_id',true);
        //$data['employee_department'] = $this->input->post('PfEmployee_id',true);
        $data['month'] = $this->input->post('month',true);
        $data['amount'] = $this->input->post('pf_amount',true);
        $this->employee_model->save_employee_Pf($data);
        $this->session->set_flashdata("addEmployeePf","addEmployeePf");
        redirect('site/add_Pf_employee');
    }
    public function add_Pfemployee(){

        $data = array();
      
        $employee_department = $this->input->post('department',true);
        $data['employee_id'] = $this->input->post('Employee',true);
        $data['Pf_no'] = $this->input->post('pf_no',true);
        
        $this->employee_model->save_Pf_employee($data);
        $this->session->set_flashdata("add_pfemployee","add_pfemployee");
        redirect('site/Pf_employee');
    }
    function add_manager(){
        $data = array();
        $data['employee_name'] = $this->input->post('name',true);
        $data['employee_department'] = $this->input->post('department',true);
        $data['employee_phone'] = $this->input->post('phone',true);
        $data['employee_city'] = $this->input->post('city',true);
        $data['employee_designation'] = $this->input->post('Designation',true);
        $data['rate'] = $this->input->post('rate',true);
        $data['type'] = 'Manager';
        $this->employee_model->save_manager($data);
        $this->session->set_flashdata("addManager","addManager");
        redirect('site/manager');
    }
    function add_gatEmployee(){
        $data = array();
        // if(empty($this->input->post('name',true))){
        //     $data['employee_name'] = $this->input->post('employee_exist',true);
        // }else{
        //     $data['employee_name'] = $this->input->post('name',true);
        // }
        $data['employee_name'] = $this->input->post('name',true);
        $data['employee_department'] = $this->input->post('department',true);
        $data['employee_phone'] = $this->input->post('phone',true);
        $data['employee_city'] = $this->input->post('city',true);
        $data['employee_designation'] = $this->input->post('Designation',true);
        $data['rate'] = $this->input->post('rate',true);
        $data['type'] = 'GaatEmployee';
        //echo "<pre>";print_r($data);exit;
        $this->employee_model->save_gatEmployee($data);
        $this->session->set_flashdata("addgaatEmployee","addgaatEmployee");
        redirect('site/gaatEmployee');
    }
    
    //move to add employee page 
    public function add_employee_page(){
        $data = array();
        $data['title'] = "Add Employee";
        $data['heading'] = "Add Employee Details";
        $data['content'] = $this->load->view('modal/add_employee');
        $this->load->view('master',$data);
    }

    public function delete_employee($employee_id){

        //$employee_id = $this->input->post('id',true);
        $this->employee_model->erase_employee($employee_id);
		$this->salary_model->delete_salary_by_id($employee_id);
        $this->session->set_flashdata("deleteEmployee","deleteEmployee");
        redirect('site/employee');
    }
    public function deletePf_employee($employeePf_id){

        //$employee_id = $this->input->post('id',true);
        $this->employee_model->erase_Pfemployee($employeePf_id);
		//$this->salary_model->delete_salary_by_id($employeePf_id);
        $this->session->set_flashdata("deletePfEmployee","deletePfEmployee");
        redirect('site/Pf_employee');
    }
    
    public function delete_manager($manager_id){

        //$employee_id = $this->input->post('id',true);
        $this->employee_model->erase_manager($manager_id);
		$deleteManager = $this->salary_model->delete_Managersalary_by_id($manager_id);
        $this->session->set_flashdata("deleteManager","deleteManager");
        redirect('site/manager');
    }
    public function delete_gatEmployee($employee_id){

        //$employee_id = $this->input->post('id',true);
        $this->employee_model->erase_gatEmployee($employee_id);
		$deleteSalary = $this->salary_model->delete_gatEmployee_by_id($employee_id);
        $this->session->set_flashdata("deletegaatEmployee","deletegaatEmployee");
        redirect('site/gaatEmployee');
    }
    public function get_employee_data($department_id){
        $this->session->set_userdata("session_department_id",$department_id);
        $department_name = $this->employee_model->getDepartmentName($department_id);
        $this->session->set_userdata("session_department_name",$department_name);
        // $this->session->set_flashdata("session_department_id","session_department_id123");
        // $session_department_id = $this->session->flashdata('session_department_id');
        // echo $this->session->userdata('session_department_id');exit;
        $data['result'] = $this->employee_model->fetch_employee_by_department($department_id);
        print_r(json_encode($data));
    } 
    public function setDateGlobally($date){
        $this->session->set_userdata("session_date",$date);
        echo $date;
    }
    public function setStartDateGlobally($date){
        $this->session->set_userdata("session_start_date",$date);
        echo $date;
    }
    public function setEndDateGlobally($date){
        $this->session->set_userdata("session_end_date",$date);
        echo $date;
    }
    public function setGloaballPfMonth($month){
        $this->session->set_userdata("session_pf_month",$month);
        echo $month;
    }
    // public function setDepartment($department_id){
    //     $this->session->set_userdata("session_department_id",$department_id);
    //     echo $this->session->userdata('session_department_id');
    // }
    public function get_employeePf_data($employeePf_id){
        $data['result'] = $this->employee_model->fetch_employeePf_by_Id($employeePf_id);
        print_r(json_encode($data));
    }
    public function get_employee_dataNew($department_id){
        
        $result = $this->employee_model->fetch_employee_by_department($department_id);
        
        $data1 = "";
        for($i=0;$i<count($result);$i++){
            $data['result'][$i]=get_object_vars($result[$i]);
            //return $data['result'][$i]['employee_id'];
            $data1 .= "<option value=".$data['result'][$i]['employee_id'].">".$data['result'][$i]['employee_name']."</option>";
        }
        echo  $data1;
        //print_r(json_encode($data));
    } 
    public function check_diamond_Available(){
        //print_r($_POST);exit;
        $data['check_diamond_Available'] = $this->employee_model->check_diamond_Available($_POST['Employee_id'],$_POST['date']);
        print_r(json_encode($data));
    }
    public function check_diamond_AvailableNew(){
        //print_r($_POST);exit;
        $data['check_diamond_Available'] = $this->employee_model->check_diamond_Available($_POST['Employee_id'],$_POST['date'],$_POST['Department_id']);
        print_r(json_encode($data));
    }
    public function get_employee_data_setTable($department_id){
        $data['result'] = $this->employee_model->fetch_employee_by_department_setTable($department_id);
        $res = json_decode(json_encode($data['result']), True);
        //foreach ($data['result'] as $value) 
        //$array[] = $value->employee_city;
        //print_r($res);
        if(!empty($data['result'])){
        $data = '<table class="table table-responsive table-striped table-advance table-hover" id="employee_Data">
        <thead>
            <tr>
                <th><i class="icon_profile"></i> No </th>
                <th><i class="icon_profile"></i> ID </th>
                <th><i class="icon_profile"></i> Full Name</th>
                <th><i class="icon_datareport_alt"></i> Department</th>
                <th><i class="icon_datareport"></i> City </th>
                <th><i class="icon_datareport"></i> Contact No </th>
                <th><i class="icon_clipboard"></i> Action</th>
            </tr>
        </thead>
        <tbody>
                    ';
                    for($i=0;$i<count($res);$i++){ 
                        $data .= '
                        <tr>
                            <td>'. ($i+1) .'</td>
                            <td>'.$res[$i]["employee_id"].'</td>
                            <td>
                                <a href="'.base_url().'employee/view_employee/'.$res[$i]["employee_id"].'">'.$res[$i]["employee_name"].'</a>
                            </td>
                            <td>'.$res[$i]["employee_department"].'</td>
                            <td>'.$res[$i]["employee_city"].'</td>
                            <td>'.$res[$i]["employee_phone"].'</td>
                            <td>
                                <a href="'.base_url().'employee/update_employee/'.$res[$i]["employee_id"].'"><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:green;font-size:20px"></i></a>
                                ';
                                if($res[$i]['employee_salary'] == 0 ) {

                                }
                                $data .='
								|
								<a  href="'.base_url().'employee/delete_employee/'.$res[$i]["employee_id"].'"><i class="fa fa-trash-o" aria-hidden="true" style="color:red;font-size:20px"></i></a>
                            </td>
                        </tr>
                    ' ; }
                    $data.= '
        </tbody>
        </table>
        
        ';
                            }else{
                                $data = '
                                <table class="table table-responsive table-striped table-advance table-hover" id="employee_Data">
                                <thead>
                                    <tr>
                                        <th><i class="icon_profile"></i> Id </th>
                                        <th><i class="icon_profile"></i> Full Name</th>
                                        <th><i class="icon_datareport_alt"></i> Department</th>
                                        <th><i class="icon_datareport"></i> City </th>
                                        <th><i class="icon_datareport"></i> Contact No </th>
                                        <th><i class="icon_clipboard"></i> Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr><td colspan="6">Employee Data is not Found for this Department</td></tr>
                                </tbody>
                                </table>';
                            }
        echo $data;
        //print_r(json_encode($data));  
    } 

    public function get_employee_dailywork($Employee_id){
        $data['dailywork'] = $this->employee_model->fetch_employee_dailywork($Employee_id);
        print_r(json_encode($data));
    }
    public function get_employee_salary($Employee_id){
        $data['Employee_id'] = $Employee_id;
        $data['start_date'] = $_POST['startDate'];
        $data['end_date'] = $_POST['endDate'];
        $data['salary'] = $this->employee_model->fetch_employee_salary($data);
        //print_r($data);exit;
        if($data['salary']==0){
            echo "0";
        }else{
            print_r(json_encode($data));
        }
        
    }
    public function gat_employee_salary(){
        //print_r($_POST);
        $data['Employee_id'] = $_POST['Employee_id'];
        //$data['Department_id'] = $_POST['Department_id'];
        $data['start_date'] = $_POST['startDate'];
        $data['end_date'] = $_POST['endDate'];
        $data['salary_data'] = $this->employee_model->fetch_gat_employee_salary($data);
        //print_r($data);exit;
        if($data['salary_data']==0){
            echo "0";
        }else{
            print_r(json_encode($data));
        }
        
    }
    
    public function add_salary(){
        $data['employee_id'] = $this->input->post('Employee_id',true);
        $data['start_date'] = $this->input->post('str_date',true);
        $data['end_date'] = $this->input->post('ed_date',true);
        $data['salary'] = $this->input->post('salary',true);
        $data['diamond'] = $this->input->post('current_month_diamond',true);
        $this->employee_model->add_employee_salary($data);
        $this->session->set_flashdata("addsalary","addsalary");
        redirect('site/salary');
    }

    public function add_manager_salary(){
        $data['employee_id'] = $this->input->post('Employee_id',true);
        $data['start_date'] = $this->input->post('str_date',true);
        $data['end_date'] = $this->input->post('ed_date',true);
        $data['salary'] = $this->input->post('salary',true);
        $data['diamond'] = $this->input->post('current_month_diamond',true);
        $this->employee_model->add_employee_salary($data);
        $this->session->set_flashdata("addsalary","addsalary");
        redirect('site/manager_salary');
    }

    public function add_gat_salary(){
        $department = $this->input->post('dept_temp',true);  
        $salary = $this->input->post('salary',true);
        $diamond = $this->input->post('current_month_diamond',true);
        //echo "<pre>";print_r($data);exit;
        for($i=0;$i<count($department);$i++){
            $data['employee_id'] = $this->input->post('Employee_id',true);
            $data['department_id'] = $department[$i];   
            $data['start_date'] = $this->input->post('str_date',true);
            $data['end_date'] = $this->input->post('ed_date',true);
            $data['salary'] = $salary[$i];
            $data['diamond'] = $diamond[$i];
            //echo "<pre>";print_r($data);exit;
            $this->employee_model->add_gat_salary($data);
        }
        
        $this->session->set_flashdata("addsalary","addsalary");
        redirect('site/gat_salary');
    }

    public function add_dailyWork(){
        
        $data['employee_id'] = $this->input->post('Employee_id',true);
        $data['date'] = $this->input->post('date',true);
        $data['today_diamond'] = $this->input->post('today_diamond',true);
        $data['total_diamond'] = $this->input->post('current_month_diamond',true)+$data['today_diamond'];
        
        $d = strtotime($data['date']);
        $data['month'] = date("M",$d);
        // echo $data['month'];exit;
        // echo $_POST['AddOREdit'];exit;
        if($_POST['AddOREdit']=='edit'){
            //echo "edit";exit;
            $this->session->set_flashdata("editDailywork","editDailywork");
            $this->employee_model->edit_employee_dailyWork($data);
        }else{
            //echo "add";exit;
            $this->session->set_flashdata("addDailywork","addDailywork");
            $this->employee_model->add_employee_dailyWork($data);
        }
        
        redirect('site/dailyWork');
        //print_r($data);exit;
    }
    public function add_gat_dailyWork(){
        
        $data['employee_id'] = $this->input->post('Employee_id',true);
        $data['department_id'] = $this->input->post('department',true);
        $data['date'] = $this->input->post('date',true);
        $data['today_diamond'] = $this->input->post('today_diamond',true);
        $data['total_diamond'] = $this->input->post('current_month_diamond',true)+$data['today_diamond'];
        $data['month'] = date('M');
        //print_r($data);exit;
        if($_POST['AddOREdit']=='edit'){
            //echo "edit";exit;
            $this->session->set_flashdata("editDailywork","editDailywork");
            $this->employee_model->edit_employee_dailyWorkNew($data);
        }else{
            //echo "add";exit;
            $this->session->set_flashdata("addDailywork","addDailywork");
            $this->employee_model->add_gat_employee_dailyWork($data);
        }
        
        redirect('site/gat_dailyWork');
        //print_r($data);exit;
    }
    public function gat_salary_report(){
        $data = array();
        $data['str_date'] = $_POST['str_date'];
        $data['ed_date'] = $_POST['ed_date'];
      
        $salaryData = $this->employee_model->fetch_gat_reportData_twoDate($data['str_date'],$data['ed_date']);
        echo "<pre>";print_r($salaryData);die;
        if(empty($salaryData)){
            $this->session->set_flashdata("dataNotFound","dataNotFound");
            redirect('site/report');
        }
        //echo "<pre>";print_r($salaryData);
        for($i=0;$i<count($salaryData);$i++){
            $data['employee'][$i] = $salaryData[$i];
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
        }
        //echo "<pre>";print_r($data['employee']);exit;
        $data['title'] = "Employee Salary Report";
        $data['heading'] = "Sanskar Diamond";
        //$data['Department_name'] = get_object_vars($data['Department_name']);
        $data['content'] = $this->load->view('Gat_Days_wise_report',$data,true);
        $this->session->set_userdata("str_date",$data['str_date']);
        $this->session->set_userdata("ed_date",$data['ed_date']);
        //$this->session->set_userdata("department_name",$data['Department_name']);
        $this->load->view('master',$data);
    }
    public function gat_report_page(){
        $data = array();
        $data['title'] = "Gat Employee Salary Report";
        $data['heading'] = "Gat Employee Salary Report";
        $data["per_page"] = 100;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["department"] = $this->department_model->fetch_department();
        // $data["department"] = $this->department_model->fetch_department($data["per_page"], $page);
        $data['content'] = $this->load->view('gat_salary_report',$data,true);
        $this->load->view('master',$data);
    }
    public function gat_report()
    {
        $data = array();
        $data['str_date'] = $_POST['str_date'];
        $data['ed_date'] = $_POST['ed_date'];
        //$this->session->set_userdata('department',$_POST["department"]);
        $data['Department_name'] = $this->department_model->fetch_department_by_id($_POST['department']);
        $salaryData = $this->employee_model->fetch_reportData_twoDate($data['str_date'],$data['ed_date'],$_POST['department']);
        if(empty($salaryData)){
            $this->session->set_flashdata("dataNotFound","dataNotFound");
            redirect('site/report');
        }
        for($i=0;$i<count($salaryData);$i++){
            $data['employee'][$i] = get_object_vars($salaryData[$i]);
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
        }
        //echo "<pre>";print_r($data['employee']);exit;
        $data['title'] = "Employee Salary Report";
        $data['heading'] = "Sanskar Diamond";
        $data['Department_name'] = get_object_vars($data['Department_name']);
        $data['content'] = $this->load->view('Days_wise_report',$data,true);
        $this->session->set_userdata("str_date",$data['str_date']);
        $this->session->set_userdata("ed_date",$data['ed_date']);
        $this->session->set_userdata("department_name",$data['Department_name']);
        $this->load->view('master',$data);
    }
    //FOR MANAGE EMPLOYEE SALARY
    public function salary_report(){
        $data = array();
        $data['str_date'] = $_POST['str_date'];
        $data['ed_date'] = $_POST['ed_date'];
        
        //$this->session->set_userdata('department',$_POST["department"]);
        $data['Department_name'] = $this->department_model->fetch_department_by_id($_POST['department']);
        //print_r($data['Department_name']);exit;
        $salaryData = $this->employee_model->fetch_reportData_twoDate($data['str_date'],$data['ed_date'],$_POST['department']);
        //print_r($salaryData);exit;
        if(empty($salaryData)){
            $this->session->set_flashdata("dataNotFound","dataNotFound");
            redirect('site/report');
        }
        for($i=0;$i<count($salaryData);$i++){
            $data['employee'][$i] = get_object_vars($salaryData[$i]);
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
        }
        //echo "<pre>";print_r($data['employee']);exit;
       
        $data['title'] = "Employee Salary Report";
        $data['heading'] = "Sanskar Diamond";
        $data['Department_name'] = get_object_vars($data['Department_name']);
        //echo "<pre>";print_r($data);exit;
        $data['content'] = $this->load->view('Days_wise_report',$data,true);
        $this->session->set_userdata("str_date",$data['str_date']);
        $this->session->set_userdata("ed_date",$data['ed_date']);
        $this->session->set_userdata("department_name",$data['Department_name']);
        $this->load->view('master',$data);
    }
    public function set_fix_deposite(){
        if($_POST["fix_deposite"]){
            $this->session->set_userdata('fix_deposite',$_POST["fix_deposite"]);
            //$this->session->sess_expiration = 10;
        }else{
            $this->session->set_userdata('fix_deposite',0);
            //$this->session->sess_expiration = 10;
        }
        echo "fix_deposite session is created";
    }
    public function salary_recipt(){
        //print_r($this->session->userdata());
        $data = array();
        $data['str_date'] = $this->session->userdata('str_date');
        $data['ed_date'] = $this->session->userdata('ed_date');
        $data['Department_name'] = $this->session->userdata('department_name');
        $data['fix_deposite'] = $_POST['fix_deposite'];
        $salaryData = $this->employee_model->fetch_reportData_twoDate($data['str_date'],$data['ed_date'], $data['Department_name']['department_id']);
        // echo "<pre>";var_dump($salaryData);exit;
        for($i=0;$i<count($salaryData);$i++){
            $data['employee'][$i] = get_object_vars($salaryData[$i]);
            //$data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
            if(!empty($data['employee'][$i]['pf'])){
                $pf = $data['employee'][$i]['pf'][0]['amount'];
            }else{
                $pf = 0;
            }
            if(!empty($data['employee'][$i]['baki'])){
                if($data['employee'][$i]['baki'][0]['baki']<$this->session->userdata('fix_deposite'))
                {
                    $baki = $data['employee'][$i]['baki'][0]['baki'];
                }else{
                    $baki = $this->session->userdata('fix_deposite');
                }
            }else{
                $baki = 0;
            }
            if(($data['employee'][$i]['salary']-$pf)<$baki){
                //$salary = ($data['employee'][$i]['salary']-$pf)-($data['employee'][$i]['salary']-$pf);
                $baki = $data['employee'][$i]['salary']-$pf;
                //echo $data['employee'][$i]['salary']."".$baki."".$pf;exit;
            }else if($baki<($data['employee'][$i]['salary'])){
                $baki = $baki;
            }else{
                $baki = $this->session->userdata('fix_deposite');
            }
            // echo "<pre>";print_r($data['employee'][$i]);exit;
            //echo $baki;exit;
            $check = $this->employee_model->update_EmployeeBaki($data['employee'][$i]['employee_id'],$baki);
            // echo $check;exit;
            //echo $check;exit;
            //print_r($data['Department_name']['department_id']);exit;
            $salaryData = $this->employee_model->fetch_reportData_twoDate($data['str_date'],$data['ed_date'],
            $data['Department_name']['department_id']);
            $data['employee'][$i] = get_object_vars($salaryData[$i]);
            //echo "<pre>";print_r($data['employee']);exit;
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
            //echo "<pre>";print_r($data['employee'][$i]);exit;
            $check_pf_status = $this->employee_model->update_EmployeePf_status($data['employee'][$i]['employee_id']);
        }
        
        //echo "<pre>";print_r($data['employee']);exit;
        $data['title'] = "Employee Salary Reciept";
        $data['heading'] = "Sanskar Diamond";

        //$data['Department_name'] = get_object_vars($data['Department_name']);
        $data['content'] = $this->load->view('employee_receipt',$data);
        // $this->session->set_userdata("str_date",$data['str_date']);
        // $this->session->set_userdata("ed_date",$data['ed_date']);
        // $this->session->set_userdata("department_name",$data['Department_name']);
        //$this->load->view('master',$data);
    }

    public function salary_recipt_gat(){
        //print_r($this->session->userdata());
        $data = array();
        $data['str_date'] = $this->session->userdata('str_date');
        $data['ed_date'] = $this->session->userdata('ed_date');
        $data['Department_name'] = $this->session->userdata('department_name');
        $data['fix_deposite'] = $_POST['fix_deposite'];
        $salaryData = $this->employee_model->fetch_reportData_twoDate_New($data['str_date'],$data['ed_date']);
        for($i=0;$i<count($salaryData);$i++){
            $data['employee'][$i] = $salaryData[$i];
            //print_r($data['employee'][$i]);exit;
            //$data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
            if(!empty($data['employee'][$i]['pf'])){
                $pf = $data['employee'][$i]['pf'][0]['amount'];
            }else{
                $pf = 0;
            }
            if(!empty($data['employee'][$i]['baki'])){
                if($data['employee'][$i]['baki'][0]['baki']<$this->session->userdata('fix_deposite'))
                {
                    $baki = $data['employee'][$i]['baki'][0]['baki'];
                }else{
                    $baki = $this->session->userdata('fix_deposite');
                }
            }else{
                $baki = 0;
            }
            $salary = 0;
            for($j=0;$j<count($data['employee'][$i]['salary']);$j++){
                $salary = $salary+$data['employee'][$i]['salary'][$j];
            }
            if(($salary-$pf)<$baki){
                //$salary = ($data['employee'][$i]['salary']-$pf)-($data['employee'][$i]['salary']-$pf);
                $baki = $salary;
            }else if($baki<($salary)){
                $baki = $baki;
            }else{
                $baki = $this->session->userdata('fix_deposite');
            }
            //echo $baki;exit;
            $this->employee_model->update_EmployeeBaki($data['employee'][$i]['employee_id'],$baki);
            $salaryData = $this->employee_model->fetch_reportData_twoDate_New($data['str_date'],$data['ed_date'],$data['Department_name']['department_id']);
            $data['employee'][$i] = $salaryData[$i];
            $data['employee'][$i]['pf'] = $this->employee_model->get_EmployeePf($data['employee'][$i]['employee_id']);
            $data['employee'][$i]['baki'] = $this->employee_model->get_EmployeeBaki($data['employee'][$i]['employee_id']);
            $this->employee_model->update_EmployeePf_status($data['employee'][$i]['employee_id']);
        }
        
        //echo "<pre>";print_r($data['employee']);exit;
        $data['title'] = "Employee Salary Reciept";
        $data['heading'] = "Sanskar Diamond";
        //$data['Department_name'] = get_object_vars($data['Department_name']);
        $data['content'] = $this->load->view('gat_employee_receipt',$data);
        // $this->session->set_userdata("str_date",$data['str_date']);
        // $this->session->set_userdata("ed_date",$data['ed_date']);
        // $this->session->set_userdata("department_name",$data['Department_name']);
        //$this->load->view('master',$data);
    }

    public function otherPay(){
        $data['employee_id'] = $this->input->post('Employee_id',true);
        $data['payment'] = $this->input->post('extraPay',true);
        $data['payment_at'] = $this->input->post('date',true);
        //print_r($data);exit;
        $this->employee_model->add_extraPay($data);
        redirect('site/extra_pay');
    }

    public  function searchemployee(){
        if(isset($_POST["query"]))  
        {  
            $data['employee'] = $this->employee_model->searchemployee($_POST["query"]);
            echo $data['employee'];  
        }
    }

    public function setemployee()
    {
        if(isset($_POST["employee_name"]))  
        {  
            $data['employee'] = $this->employee_model->searchemployee1($_POST["employee_name"]);
            echo $data['employee'];  
        }
    }

    /* Below APIs are application side */
    
    public function login_post() {
        echo "parth";exit;
        // Get the post data
        $email = $this->post('email');
        $password = $this->post('password');
        
        // Validate the post data
        if(!empty($email) && !empty($password)){
            
            // Check if any user exists with the given credentials
            $con['returnType'] = 'single';
            $con['conditions'] = array(
                'email' => $email,
                'password' => md5($password),
                'status' => 1
            );
            $user = $this->user->getRows($con);
            
            if($user){
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User login successful.',
                    'data' => $user
                ], REST_Controller::HTTP_OK);
            }else{
                // Set the response and exit
                //BAD_REQUEST (400) being the HTTP response code
                $this->response("Wrong email or password.", REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            // Set the response and exit
            $this->response("Provide email and password.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}